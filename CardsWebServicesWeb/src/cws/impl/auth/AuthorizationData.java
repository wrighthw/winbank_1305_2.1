package cws.impl.auth;

import java.math.BigDecimal;
import java.util.Date;

import cws.impl.CWServer;
import cws.impl.MessageImpl;
import cws.impl.ParameterDictionary;

public class AuthorizationData {
	int recordCount;
	String participantID;		 // IDTAPART  CHAR        3    Participant ID                   
	String accountNo;			 // TAACCT    CHAR       19    Account #                        
	String currencyCode;		 // TACURC    CHAR        3    Currency Code                     
	String referenceNo;			 // TAREFN    CHAR       12    Reference #                      
	String approvalNo;			 // TAAPRN    CHAR        6    Approval #                       	
	int transDate;				 // TATDAT    DECIMAL     7    Transaction Date    	             
	int transTime;				 // TATTIM    DECIMAL     6    Transaction Time
		Date transDateTimeMsg;	                 	
	String postingClass;		 // TAPSCL    CHAR        2    Posting Class
	String postingClassDescrGR;
	String postingClassDescrEN;
	String debitCredit;			 // TADBCR    CHAR        1    Debit/Credit                     
	BigDecimal transAmount;		 // TAAMT     DECIMAL    15    Transaction Amount 
		BigDecimal transAmountMsg;              
	BigDecimal transActualAmount;// TAAAMT    DECIMAL    15    Transaction Actual Amount
		BigDecimal transActualAmountMsg;        
	String cardNo;				 // TACARD    CHAR       19    Card #                         
	String authDescription;		 // TADESC    CHAR       40    Authorization Description
	
	public AuthorizationData() {      
	}
	
	public int getRecordCount() {	
		return recordCount;
	}
	
	public String getAccountNo() {
		return accountNo;
	}

	public String getApprovalNo() {
		return approvalNo;
	}

	public String getAuthDescription() {
		return authDescription;
	}

	public String getCardNo() {
		return cardNo;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public String getDebitCredit() {
		return debitCredit;
	}

	public String getParticipantID() {
		return participantID;
	}

	public String getPostingClass() {
		return postingClass;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public BigDecimal getTransActualAmount() {
		return transActualAmountMsg;
	}

	public BigDecimal getTransAmount() {
		return transAmountMsg;
	}

	public Date getTransDateTime() {
		return transDateTimeMsg;
	}

	public String getPostingClassDescrEN() {
		return postingClassDescrEN;
	}

	public String getPostingClassDescrGR() {
		return postingClassDescrGR;
	}
	
	public void transform() {
		transDateTimeMsg = MessageImpl.decodeDateTime(transDate, transTime);
		transActualAmountMsg = MessageImpl.decodeAmount(transActualAmount, MessageImpl.getDecimalPlaces());
		transAmountMsg = MessageImpl.decodeAmount(transAmount, MessageImpl.getDecimalPlaces());
		
		ParameterDictionary dict = CWServer.getInstance().getDictionary();
		postingClassDescrGR = dict.getPostingClassGR(postingClass);  
		postingClassDescrEN = dict.getPostingClassEN(postingClass);		
	}
}
