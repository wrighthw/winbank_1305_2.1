package cws.impl.auth;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import cws.impl.CWServer;
import cws.impl.MessageImpl;
import cws.impl.ResultSetIterator;

/*

Authorisations

Input

MSGNUM  NUMERIC     2    2 
TAPART  CHAR        3    Participant ID                   
TAACCT  CHAR       19    Account #                        
TACURC  CHAR        3    Currency Code                     

Output

TAPART  CHAR        3    Participant ID                   
TAACCT  CHAR       19    Account #                        
TACURC  CHAR        3    Currency Code                     
TAREFN  CHAR       12    Reference #                      
TAAPRN  CHAR        6    Approval #                       
TATDAT  DECIMAL     7    Transaction Date                 
TATTIM  DECIMAL     6    Transaction Time                 
TAPSCL  CHAR        2    Posting Class                    
TADBCR  CHAR        1    Debit/Credit                     
TAAMT   DECIMAL    15    Transaction Amount               
TAAAMT  DECIMAL    15    Transaction Actual Amount        
TACARD  CHAR       19    Card #                         
TADESC  CHAR       40    Authorization Description      

 
File : CTRNAU0P

*/

public class AuthorizationImpl extends MessageImpl {

	public static AuthorizationData[] execute(
		String participantID, // TAPART  CHAR        3    Participant ID                   
		String accountNo,     // TAACCT  CHAR       19    Account # 
		String cardNo,     	  // TACARD  CHAR       19    Card #
		String currencyCode,  // TACURC  CHAR        3    Currency Code
		boolean sortByCardNo,
		int start,            // 1-based
		int count		
	) 
	throws SQLException {
		
		return execute(participantID, accountNo, cardNo, currencyCode, null, null, sortByCardNo, start, count);		
	}

	public static AuthorizationData[] execute(
		String participantID, // TAPART  CHAR        3    Participant ID                   
		String accountNo,     // TAACCT  CHAR       19    Account #
		String cardNo,     	  // TACARD  CHAR       19    Card #                        
		String currencyCode,  // TACURC  CHAR        3    Currency Code
		Date dateFrom,			  			  
		Date dateTo,		
		boolean sortByCardNo,		
		int start,            // 1-based
		int count		
	) 
	throws SQLException {

		/*
		participantID = "002";                   
		accountNo = "4908450000377004";    
		cardNo = "";                    
		currencyCode = "978";
		sortByCardNo = false;
		start = 1;
		count = 10;
		*/		

		AuthorizationData[] data = new AuthorizationData[count]; 
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs  = null;
		int index = 0;
		try {
			con = CWServer.getInstance().getAS400().getDBConnection();
			
			/* count rows */
			
			String sql = "SELECT COUNT(*)"
				+ " FROM " + getCardsLibrary() + ".CTRNAU0P"
				+ " WHERE TAPART = ? AND TAACCT = ? AND TACURC = ?";
			if (dateFrom != null && dateTo != null)
				sql += " AND TATDAT >= ? AND TATDAT <= ?";
			if (cardNo != null)
				sql += " AND TACARD = ?"; 
												
			ps = con.prepareStatement(sql);	
			int pos = 1;
			ps.setString(pos, participantID); pos++;
			ps.setString(pos, accountNo); pos++;
			ps.setString(pos, currencyCode); pos++;
			if (dateFrom != null && dateTo != null) {
				ps.setInt(pos, encodeDate(dateFrom)); pos++;
				ps.setInt(pos, encodeDate(dateTo)); pos++;
			}
			if (cardNo != null) {			
				ps.setString(pos, cardNo); pos++;
			}
				
			rs = ps.executeQuery();
			rs.next();
			int recordCount = rs.getInt(1);			
			
			/* get actual data */
			
			sql = "SELECT"					 
				+ " TAPART, TAACCT, TACURC, TAREFN, TAAPRN, TATDAT, TATTIM,"
				+ " TAPSCL, TADBCR, TAAMT, TAAAMT, TACARD, TADESC"				
				+ " FROM " + getCardsLibrary() + ".CTRNAU0P"
				+ " WHERE TAPART = ? AND TAACCT = ? AND TACURC = ?";
			if (dateFrom != null && dateTo != null)
				sql += " AND TATDAT >= ? AND TATDAT <= ?";	
			if (cardNo != null)
				sql += " AND TACARD = ?"; 												
			sql += " ORDER BY";
			if (sortByCardNo)
				sql += " TACARD ASC,";
			sql += " TATDAT DESC, TATTIM DESC";
			
			ps = con.prepareStatement(sql);		
			pos = 1;	
			ps.setString(pos, participantID); pos++;
			ps.setString(pos, accountNo); pos++;
			ps.setString(pos, currencyCode); pos++;
			if (dateFrom != null && dateTo != null) {
				ps.setInt(pos, encodeDate(dateFrom)); pos++;
				ps.setInt(pos, encodeDate(dateTo)); pos++;
			}
			if (cardNo != null) {			
				ps.setString(pos, cardNo); pos++;
			}
						
			rs = ps.executeQuery();						
			
			AuthorizationData entry;			
			ResultSetIterator iter = new ResultSetIterator(rs, start, count);
			if (iter.hasRows()) {
				do {
					entry = new AuthorizationData();
					
					entry.recordCount 		= recordCount;
					entry.participantID 	= handleNull(rs.getString("TAPART"));                   
					entry.accountNo 		= handleNull(rs.getString("TAACCT"));               
					entry.currencyCode 		= handleNull(rs.getString("TACURC"));                     
					entry.referenceNo 		= handleNull(rs.getString("TAREFN"));                      
					entry.approvalNo 		= handleNull(rs.getString("TAAPRN"));                       
					entry.transDate 		= rs.getInt("TATDAT");                 
					entry.transTime 		= rs.getInt("TATTIM");                 
					entry.postingClass 		= handleNull(rs.getString("TAPSCL"));
					entry.debitCredit 		= handleNull(rs.getString("TADBCR"));                     
					entry.transAmount 		= handleNull(rs.getBigDecimal("TAAMT"));               
					entry.transActualAmount = handleNull(rs.getBigDecimal("TAAAMT"));        
					entry.cardNo 			= handleNull(rs.getString("TACARD"));                         
					entry.authDescription 	= handleNull(rs.getString("TADESC"));
					
					entry.transform();
									
					data[index] = entry;
					index++;
				} while (iter.next());
			}	 			
		}
		finally {
			closeObjects(con, ps, rs);
		}
		
		AuthorizationData[] trimmedData = new AuthorizationData[index];		
		System.arraycopy(data, 0, trimmedData, 0, index);				  
		return trimmedData;
	}
}
