package cws.impl.auth;


import cws.MessageResponder;
import cws.XmlTools;
import cws.impl.Statistics;
import cws.log.Logger;

public class AuthorizationResponder extends MessageResponder {

	public String processRequest(String message, String dataXML, String extraDataXML) throws Exception {	

//		Date date1 = new Date();

		int[] positions = new int[2];
		
		String participantID = XmlTools.parseTag(dataXML, "participantID", positions);		
		String accountNo = XmlTools.parseTag(dataXML, "accountNo", positions);
		String cardNo = XmlTools.parseTag(dataXML, "cardNo", positions);
		String currencyCode = XmlTools.parseTag(dataXML, "currencyCode", positions);
		boolean sortByCardNo = Boolean.valueOf(XmlTools.parseTag(dataXML, "sortByCardNo", positions)).booleanValue();
		int start = Integer.parseInt(XmlTools.parseTag(dataXML, "start", positions));
		int count = Integer.parseInt(XmlTools.parseTag(dataXML, "count", positions));
				 											
		AuthorizationData[] rows = getAuthorizations(participantID, accountNo, cardNo, 
														currencyCode, sortByCardNo, start, count);															
		String responseXML = generateResponseXML(rows);		
		
//		Date date2 = new Date();	
//		
//		Logger.logTimeAndRequestMessage("getAuthorizations","cardNo:"+cardNo, date1, date2);
				
		return responseXML;		
	}

	protected AuthorizationData[] getAuthorizations(String participantID, String accountNo, String cardNo, 
														String currencyCode, 
														boolean sortByCardNo,
														int start, int count) 
	throws Exception {
		
		AuthorizationData[] authorizations = null;
		try {
			long startTime = System.currentTimeMillis();
			if (cardNo != null && (cardNo.equals("") || cardNo.equals(" ")))
				cardNo = null;
			authorizations = AuthorizationImpl.execute(participantID, accountNo, cardNo, currencyCode,
														sortByCardNo, start, count);
			long endTime = System.currentTimeMillis();
			Statistics.addRequest("getAuthorizations", endTime - startTime);
		}
		catch (Exception ex) {
			Logger.logException(ex);
			authorizations = new AuthorizationData[0]; 
		}
											
		return authorizations; 
	}

	protected String generateResponseXML(AuthorizationData[] rows) {	
		StringBuffer multiRefsXML = new StringBuffer();

		int count = rows.length;
		for (int i = 0; i < count; i++)
			appendArrayItem(multiRefsXML, i, rows[i]);
		
		return generateResponseXMLArray("getAuthorizationsResponse", "getAuthorizationsReturn",  
											"http://auth.impl.cws",
											"AuthorizationData", count, multiRefsXML.toString());			
	}

	protected void appendArrayItem(StringBuffer dataXML, int id, AuthorizationData data) {
		
		appendMultiRefStart(dataXML, id, "AuthorizationData");
								
		appendInteger(dataXML, "recordCount", data.getRecordCount());
		appendString(dataXML, "accountNo", data.getAccountNo());
		appendString(dataXML, "approvalNo", data.getApprovalNo());
		appendString(dataXML, "authDescription", encodeString(data.getAuthDescription()));
		appendString(dataXML, "cardNo", data.getCardNo());
		appendString(dataXML, "currencyCode", data.getCurrencyCode());
		appendString(dataXML, "debitCredit", data.getDebitCredit());
		appendString(dataXML, "participantID", data.getParticipantID());
		appendString(dataXML, "postingClass", data.getPostingClass());
		appendString(dataXML, "referenceNo", data.getReferenceNo());
		appendDecimal(dataXML, "transActualAmount", data.getTransActualAmount());
		appendDecimal(dataXML, "transAmount", data.getTransAmount());
		appendDate(dataXML, "transDateTime", data.getTransDateTime());
		appendString(dataXML, "postingClassDescrEN", encodeString(data.getPostingClassDescrEN()));
		appendString(dataXML, "postingClassDescrGR", encodeString(data.getPostingClassDescrGR()));
			 										 
		appendMultiRefEnd(dataXML, "AuthorizationData");
	}
}
