package cws.impl.bill;

import java.util.Date;

import cws.MessageResponder;
import cws.XmlTools;
import cws.impl.Statistics;
import cws.log.Logger;

public class BillingResponder extends MessageResponder {

	public String processRequest(String message, String dataXML, String extraDataXML) throws Exception {	


//		Date date1 = new Date();
		
		int[] positions = new int[2];
		
		String participantID = XmlTools.parseTag(dataXML, "participantID", positions);				
		String accountNo = XmlTools.parseTag(dataXML, "accountNo", positions);
		String billCurrencyCode = XmlTools.parseTag(dataXML, "billCurrencyCode", positions);
		Date billingDateLessThan = stringToDate(XmlTools.parseTag(dataXML, "billingDateLessThan", positions));
		int start = Integer.parseInt(XmlTools.parseTag(dataXML, "start", positions));
		int count = Integer.parseInt(XmlTools.parseTag(dataXML, "count", positions));																
						 													
		BillingData[] rows = getBillings(participantID, accountNo, billCurrencyCode, billingDateLessThan,
											start, count);															
		
//		//pd:20071004
//		StringBuffer trace = new StringBuffer();
//		String newLine = System.getProperty("line.separator");
//		trace.append("== getBillings ====================================").append(newLine);
//
//		for(int i =0;i<rows.length;i++)
//		{
//			trace.append("Bill: ").append(rows[i].toString());
//		}
//		Logger.logInfo("CWS:getBillings:Bills", trace.toString());
		//pd:20071004	
															
		String responseXML = generateResponseXML(rows);		
		
//		Date date2 = new Date();	
//		
//		Logger.logTimeAndRequestMessage("getBillings","accountNo:"+accountNo, date1, date2);
		
		return responseXML;		
	}

	protected BillingData[] getBillings(String participantID, String accountNo, 
											String billCurrencyCode, Date billingDateLessThan,
											int start, int count) 
	throws Exception {
		
		//pd:20071004
		StringBuffer trace = new StringBuffer();
		String newLine = System.getProperty("line.separator");
		trace.append("== getBillings ====================================").append(newLine);
		trace.append("participantID: ").append(participantID.toString()).append(newLine);
		trace.append("accountNo: ").append(accountNo.toString()).append(newLine);
		trace.append("billCurrencyCode: ").append(billCurrencyCode.toString()).append(newLine);
		trace.append("billingDateLessThan: ").append(billingDateLessThan.toString()).append(newLine);
		trace.append("start: ").append(""+start).append(newLine);
		trace.append("count: ").append(""+count).append(newLine);
		Logger.logInfo("CWS:getBillings:parameters", trace.toString());
		//pd:20071004		
		
		
		BillingData[] billings = null;
		try {
			long startTime = System.currentTimeMillis();
			billings = BillingImpl.execute(participantID, accountNo, billCurrencyCode, billingDateLessThan,
											start, count);
			long endTime = System.currentTimeMillis();
			Statistics.addRequest("getBillings", endTime - startTime);		
		}
		catch (Exception ex) {
			Logger.logException(ex);
			billings = new BillingData[0];
		}
											
		return billings; 
	}	

	protected String generateResponseXML(BillingData[] rows) {	
		StringBuffer multiRefsXML = new StringBuffer();

		int count = rows.length;
		for (int i = 0; i < count; i++)
			appendArrayItem(multiRefsXML, i, rows[i]);
		
		return generateResponseXMLArray("getBillingsResponse", "getBillingsReturn",  
											"http://bill.impl.cws",
											"BillingData", count, multiRefsXML.toString());			
	}

	protected void appendArrayItem(StringBuffer dataXML, int id, BillingData data) {
		
		appendMultiRefStart(dataXML, id, "BillingData");
								
		appendString(dataXML, "accountNo", data.getAccountNo());
		appendString(dataXML, "accountStatus", data.getAccountStatus());
		appendDecimal(dataXML, "balanceToday", data.getBalanceToday());
		appendDate(dataXML, "billDate", data.getBillDate());
		appendString(dataXML, "currencyCode", data.getCurrencyCode());
		appendDecimal(dataXML, "intRateBalTrans", data.getIntRateBalTrans());
		appendDecimal(dataXML, "intRatePurchase", data.getIntRatePurchase());
		appendDecimal(dataXML, "minPayment", data.getMinPayment());
		appendDate(dataXML, "nextPayDate", data.getNextPayDate());
		appendString(dataXML, "participantID", data.getParticipantID());
		appendInteger(dataXML, "pastDuePayCount", data.getPastDuePayCount());
		appendDecimal(dataXML, "prevBalance", data.getPrevBalance());
		appendString(dataXML, "stmtMessage1", encodeString(data.getStmtMessage1()));
		appendString(dataXML, "stmtMessage2", encodeString(data.getStmtMessage2()));
		appendString(dataXML, "stmtMessage3", encodeString(data.getStmtMessage3()));
		appendInteger(dataXML, "recordCount", data.getRecordCount());
										 
		appendMultiRefEnd(dataXML, "BillingData");	
	}
}
