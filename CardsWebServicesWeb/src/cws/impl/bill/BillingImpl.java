package cws.impl.bill;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.GregorianCalendar;

import cws.impl.CWServer;
import cws.impl.MessageImpl;
import cws.impl.ResultSetIterator;

/*

4-Billing

Input

MSGNUM  NUMERIC     2     4 
BHPART  CHAR        3     Participant ID          
BHACCT  CHAR       19     Account Number  
BHCURC  CHAR        3     Currency Code              
        DECIMAL     7     BillingDateLessThan
        NUMERIC     2     MaximumNumberOfBillings  
                

Output

BHPART  CHAR        3     Participant ID          
BHACCT  CHAR       19     Account Number    
BHCURC  CHAR        3     Currency Code              
BHASTT  CHAR        2     Account Status          
BHTBAL  DECIMAL    15     Balance Today           
BHPPBL  DECIMAL    15     Previous Balance        
BHMNPY  DECIMAL    15     Min Payment
BHPDPN  DECIMAL     3     # Payments Past Due   
BHNPDY  DECIMAL     7     Next Pay Date               
BHBLDY  DECIMAL     7     Billing Date                             
BHIRT1  DECIMAL     9 8   Interest Rate Purchase 
BHIRT9  DECIMAL     9 8   Interest Rate Balance Transfer
        CHAR       75     Statement Message 1
        CHAR       75     Statement Message 2
        CHAR       75     Statement Message 3

We do not have the messages for every billing , it will be implemented in a later phase. 
Anyway we should include them in the message and we will populate them later, 
when we will have this info.
         
File : CBILHS1L

*/

public class BillingImpl extends MessageImpl {

	public static BillingData[] execute(
		String participantID, 		// BHPART  CHAR        3    Participant ID                   
		String accountNo,     		// BHACCT  CHAR       19    Account #                        		
		String currencyCode, 		// BHCURC  CHAR        3    Account Billing Curr Code
		Date billingDateLessThan,					  
		int start,            		// 1-based
		int count		
	) 
	throws SQLException {

		/*
		participantID = "002";                   
		accountNo = "4020682000093008";                       
		currencyCode = "978";
		billingDateLessThan = 1040501;			  
		start = 1;
		count = 5;
		*/		

		// set earliest date of valid data for billing headers
		GregorianCalendar calendar = new GregorianCalendar(2003, 9, 2);
		Date billingDateGreaterThan = calendar.getTime();

		BillingData[] data = new BillingData[count]; 
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs  = null;
		int index = 0;
		try {
			con = CWServer.getInstance().getAS400().getDBConnection();
		
			/* count rows */
			
			String sql = "SELECT COUNT(*)"
				+ " FROM " + getCardsLibrary() + ".CBILHS1L"
				+ " WHERE BHPART = ? AND BHACCT = ? AND BHCURC = ? AND BHNPDY < ? AND BHNPDY > ?";				
			ps = con.prepareStatement(sql);	
			ps.setString(1, participantID);
			ps.setString(2, accountNo);
			ps.setString(3, currencyCode);
			ps.setInt(4, encodeDate(billingDateLessThan));
			ps.setInt(5, encodeDate(billingDateGreaterThan));
			rs = ps.executeQuery();
			rs.next();
			int recordCount = rs.getInt(1);			
			
			/* get actual data */		
			
			sql = "SELECT "							
				+ " BHPART, BHACCT, BHCURC, BHASTT, BHTBAL, BHPPBL, BHMNPY,"
				+ " BHPDPN, BHNPDY, BHBLDY, BHIRT1, BHIRT9"
				+ " FROM " + getCardsLibrary() + ".CBILHS1L"
				+ " WHERE BHPART = ? AND BHACCT = ? AND BHCURC = ? AND BHBLDY < ? AND BHNPDY > ?"
				+ " ORDER BY BHBLDY DESC";
			ps = con.prepareStatement(sql);			
			ps.setString(1, participantID);
			ps.setString(2, accountNo);
			ps.setString(3, currencyCode);
			ps.setInt(4, encodeDate(billingDateLessThan));
			ps.setInt(5, encodeDate(billingDateGreaterThan));
			rs = ps.executeQuery();			
			
			BillingData entry;			
			ResultSetIterator iter = new ResultSetIterator(rs, start, count);
			if (iter.hasRows()) {
				do {
					entry = new BillingData();
					
					entry.recordCount 		= recordCount;					
					entry.participantID 	= handleNull(rs.getString("BHPART"));          
					entry.accountNo 		= handleNull(rs.getString("BHACCT"));    
					entry.currencyCode 		= handleNull(rs.getString("BHCURC"));              
					entry.accountStatus 	= handleNull(rs.getString("BHASTT"));          
					entry.balanceToday 		= handleNull(rs.getBigDecimal("BHTBAL"));           
					entry.prevBalance 		= handleNull(rs.getBigDecimal("BHPPBL"));        
					entry.minPayment 		= handleNull(rs.getBigDecimal("BHMNPY"));
					entry.pastDuePayCount 	= rs.getInt("BHPDPN");   
					entry.nextPayDate 		= rs.getInt("BHNPDY");               
					entry.billDate 			= rs.getInt("BHBLDY");                             
					entry.intRatePurchase 	= handleNull(rs.getBigDecimal("BHIRT1")); 
					entry.intRateBalTrans 	= handleNull(rs.getBigDecimal("BHIRT9"));
					entry.stmtMessage1 		= handleNull((String) null);
					entry.stmtMessage2 		= handleNull((String) null);
					entry.stmtMessage3 		= handleNull((String) null);
						          
					entry.transform();	          
						              									
					data[index] = entry;
					index++;
				} while (iter.next());
			}	 			
		}
		finally {
			closeObjects(con, ps, rs);
		}
		
		BillingData[] trimmedData = new BillingData[index];		
		System.arraycopy(data, 0, trimmedData, 0, index);				  
		return trimmedData;
	}
}
