package cws.impl.bill;

import java.math.BigDecimal;
import java.util.Date;

import cws.impl.MessageImpl;

public class BillingData {
	int recordCount;
	String participantID;		// BHPART  CHAR        3     Participant ID          
	String accountNo; 			// BHACCT  CHAR       19     Account Number    
	String currencyCode;		// BHCURC  CHAR        3     Currency Code              
	String accountStatus;		// BHASTT  CHAR        2     Account Status          
	BigDecimal balanceToday;	// BHTBAL  DECIMAL    15     Balance Today
		BigDecimal balanceTodayMsg;	           
	BigDecimal prevBalance;		// BHPPBL  DECIMAL    15     Previous Balance
		BigDecimal prevBalanceMsg;	        
	BigDecimal minPayment;		// BHMNPY  DECIMAL    15     Min Payment
		BigDecimal minPaymentMsg;
	int pastDuePayCount;		// BHPDPN  DECIMAL     3     # Payments Past Due   
	int nextPayDate;			// BHNPDY  DECIMAL     7     Next Pay Date
		Date nextPayDateMsg;               
	int billDate;				// BHBLDY  DECIMAL     7     Billing Date
		Date billDateMsg;                         
	BigDecimal intRatePurchase; // BHIRT1  DECIMAL     9 8   Interest Rate Purchase
		BigDecimal intRatePurchaseMsg; 	 
	BigDecimal intRateBalTrans; // BHIRT9  DECIMAL     9 8   Interest Rate Balance Transfer
		BigDecimal intRateBalTransMsg;	
	String stmtMessage1;		//         CHAR       75     Statement Message 1
	String stmtMessage2;		//         CHAR       75     Statement Message 2
	String stmtMessage3;		//         CHAR       75     Statement Message 3
	
	public BillingData() {		
	}
	
	public String getAccountNo() {
		return accountNo;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public BigDecimal getBalanceToday() {
		return balanceTodayMsg;
	}

	public Date getBillDate() {
		return billDateMsg;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public BigDecimal getIntRateBalTrans() {
		return intRateBalTransMsg;
	}

	public BigDecimal getIntRatePurchase() {
		return intRatePurchaseMsg;
	}

	public BigDecimal getMinPayment() {
		return minPaymentMsg;
	}

	public Date getNextPayDate() {
		return nextPayDateMsg;
	}

	public String getParticipantID() {
		return participantID;
	}

	public int getPastDuePayCount() {
		return pastDuePayCount;
	}

	public BigDecimal getPrevBalance() {
		return prevBalanceMsg;
	}

	public String getStmtMessage1() {
		return stmtMessage1;
	}

	public String getStmtMessage2() {
		return stmtMessage2;
	}

	public String getStmtMessage3() {
		return stmtMessage3;
	}
	
	public int getRecordCount() {
		return recordCount;
	}
	
	public void transform() {		
		balanceTodayMsg = MessageImpl.decodeAmount(balanceToday, MessageImpl.getDecimalPlaces());
		prevBalanceMsg = MessageImpl.decodeAmount(prevBalance, MessageImpl.getDecimalPlaces());
		minPaymentMsg = MessageImpl.decodeAmount(minPayment, MessageImpl.getDecimalPlaces());
		nextPayDateMsg = MessageImpl.decodeDate(nextPayDate);               
		billDateMsg = MessageImpl.decodeDate(billDate);                         
		intRatePurchaseMsg = MessageImpl.decodeAmount(intRatePurchase, MessageImpl.getDecimalPlaces());
		intRateBalTransMsg = MessageImpl.decodeAmount(intRateBalTrans, MessageImpl.getDecimalPlaces());		
	}
}
