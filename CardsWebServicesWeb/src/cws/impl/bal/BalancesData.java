package cws.impl.bal;

import java.math.BigDecimal;
import java.util.Date;

import cws.impl.CWServer;
import cws.impl.MessageImpl;
import cws.impl.ParameterDictionary;

public class BalancesData {
	int recordCount;
	String participantID;				// AMPART  CHAR        3    Participant ID    
	String accountNo;					// AMACCT  CHAR       19    Account #  
	String currencyCode;				// AMCURC  CHAR        3    Currency Code                          
	String accountStatus;				// AMSTAT  CHAR        2    Account Status        
	String customerRel;					// AMCREL  CHAR       10    Customer Relationship   
	String affinityCode;				// AMAFFC  CHAR        3    Affinity Code    
	String cardAccountType;				// AMTYPE  CHAR        3    Card Account Type
	String cardAccountTypeDescrGR;	                  
	String cardAccountTypeDescrEN;
	int lastPayDate; 					// AMLPYD  DECIMAL     7    Last Payment Date  
		Date lastPayDateMsg;          
	BigDecimal lastPayAmount;			// AMLPYA  DECIMAL    15    Last Payment Amount
		BigDecimal lastPayAmountMsg;
	int expiredPayCount;				// AMEXPN  DECIMAL     3    # of Expired Payments          
	BigDecimal expiredPayAmount;		// AMEXPA  DECIMAL    15    Amount of Expired Payments
		BigDecimal expiredPayAmountMsg; 
	BigDecimal minPayPrincipleBal;		// AMMPYP  DECIMAL    15    Min Paym on Principle Bal
		BigDecimal minPayPrincipleBalMsg;     
	BigDecimal minPayLateCharge;		// AMMPYL  DECIMAL    15    Min Paym Late Charge              
		BigDecimal minPayLateChargeMsg;
	BigDecimal minPayInterstBal;		// AMMPYI  DECIMAL    15    Min Paym on Interest Bal
		BigDecimal minPayInterstBalMsg;   
	BigDecimal previousStmtBal;			// AMPSBL  DECIMAL    15    Previous Statement Balance
		BigDecimal previousStmtBalMsg;
	int payDueDate;						// AMCPYD  DECIMAL     7    Payment Due Date
		Date payDueDateMsg;     
	String primaryAdditional;			// CHPRIM  CHAR        1    Primary/Additional Card    
	BigDecimal creditLimit;				// 		   DECIMAL    15    Credit Limit
		BigDecimal creditLimitMsg;
	BigDecimal spendLimit;				//		   DECIMAL    15    Spending Limit
		BigDecimal spendLimitMsg;
	BigDecimal availSpendLimit;			// 		   DECIMAL    15    Available Spending Limit
		BigDecimal availSpendLimitMsg;
	BigDecimal cashAdvLimitLocal;		// 		   DECIMAL    15    Cash Advance Limit Local
		BigDecimal cashAdvLimitLocalMsg;
	BigDecimal cashAdvLimit;			//		   DECIMAL    15    Cash Advance Limit
		BigDecimal cashAdvLimitMsg;
	BigDecimal availCashAdvLimitLocal;	//		   DECIMAL    15    Available Cash Advance Limit Local
		BigDecimal availCashAdvLimitLocalMsg;
	BigDecimal availCashAdvLimit;		// 		   DECIMAL    15    Available Cash Advance Limit
		BigDecimal availCashAdvLimitMsg;
	BigDecimal currBalanceNoAuth;		//         DECIMAL	  15	Current Balance without Authorisations
		BigDecimal currBalanceNoAuthMsg;
	BigDecimal totalOutstandAuth;	//  	   DECIMAL 	  15    Total Outstanding Authorisation
		BigDecimal totalOutstandAuthMsg;

	public BalancesData() { 
	}

	public int getRecordCount() {	
		return recordCount;
	}
	
	public String getAccountNo() {
		return accountNo;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public String getAffinityCode() {
		return affinityCode;
	}

	public BigDecimal getAvailCashAdvLimit() {
		return availCashAdvLimitMsg;
	}

	public BigDecimal getAvailCashAdvLimitLocal() {
		return availCashAdvLimitLocalMsg;
	}

	public BigDecimal getAvailSpendLimit() {
		return availSpendLimitMsg;
	}

	public String getCardAccountType() {
		return cardAccountType;
	}

	public BigDecimal getCashAdvLimit() {
		return cashAdvLimitMsg;
	}

	public BigDecimal getCashAdvLimitLocal() {
		return cashAdvLimitLocalMsg;
	}

	public BigDecimal getCreditLimit() {
		return creditLimitMsg;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public String getCustomerRel() {
		return customerRel;
	}

	public BigDecimal getExpiredPayAmount() {
		return expiredPayAmountMsg;
	}

	public int getExpiredPayCount() {
		return expiredPayCount;
	}

	public BigDecimal getLastPayAmount() {
		return lastPayAmountMsg;
	}

	public Date getLastPayDate() {
		return lastPayDateMsg;
	}

	public BigDecimal getMinPayInterstBal() {
		return minPayInterstBalMsg;
	}

	public BigDecimal getMinPayLateCharge() {
		return minPayLateChargeMsg;
	}

	public BigDecimal getMinPayPrincipleBal() {
		return minPayPrincipleBalMsg;
	}

	public String getParticipantID() {
		return participantID;
	}

	public Date getPayDueDate() {
		return payDueDateMsg;
	}

	public BigDecimal getPreviousStmtBal() {
		return previousStmtBalMsg;
	}

	public String getPrimaryAdditional() {
		return primaryAdditional;
	}

	public BigDecimal getSpendLimit() {
		return spendLimitMsg;
	}
	
	public String getCardAccountTypeDescrEN() {
		return cardAccountTypeDescrEN;
	}

	public String getCardAccountTypeDescrGR() {
		return cardAccountTypeDescrGR;
	}

	public BigDecimal getCurrBalanceNoAuth() {	
		return currBalanceNoAuthMsg;
	}
	
	public BigDecimal getTotalOutstandAuth() {	
		return totalOutstandAuthMsg;
	}
	
	public void transform() {
		lastPayDateMsg = MessageImpl.decodeDate(lastPayDate);
		lastPayAmountMsg = MessageImpl.decodeAmount(lastPayAmount, MessageImpl.getDecimalPlaces());
		expiredPayAmountMsg = MessageImpl.decodeAmount(expiredPayAmount, MessageImpl.getDecimalPlaces());
		minPayPrincipleBalMsg = MessageImpl.decodeAmount(minPayPrincipleBal, MessageImpl.getDecimalPlaces());              
		minPayLateChargeMsg = MessageImpl.decodeAmount(minPayLateCharge, MessageImpl.getDecimalPlaces());
		minPayInterstBalMsg = MessageImpl.decodeAmount(minPayInterstBal, MessageImpl.getDecimalPlaces());
		previousStmtBalMsg = MessageImpl.decodeAmount(previousStmtBal, MessageImpl.getDecimalPlaces());
		payDueDateMsg = MessageImpl.decodeDate(payDueDate);     
		creditLimitMsg = MessageImpl.decodeAmount(creditLimit, MessageImpl.getDecimalPlaces());
		spendLimitMsg = MessageImpl.decodeAmount(spendLimit, MessageImpl.getDecimalPlaces());
		availSpendLimitMsg = MessageImpl.decodeAmount(availSpendLimit, MessageImpl.getDecimalPlaces());
		cashAdvLimitLocalMsg = MessageImpl.decodeAmount(cashAdvLimitLocal, MessageImpl.getDecimalPlaces());
		cashAdvLimitMsg = MessageImpl.decodeAmount(cashAdvLimit, MessageImpl.getDecimalPlaces());
		availCashAdvLimitLocalMsg = MessageImpl.decodeAmount(availCashAdvLimitLocal, MessageImpl.getDecimalPlaces());
		availCashAdvLimitMsg = MessageImpl.decodeAmount(availCashAdvLimit, MessageImpl.getDecimalPlaces());
		currBalanceNoAuthMsg = MessageImpl.decodeAmount(currBalanceNoAuth, MessageImpl.getDecimalPlaces());
		totalOutstandAuthMsg = MessageImpl.decodeAmount(totalOutstandAuth, MessageImpl.getDecimalPlaces());
		
		ParameterDictionary dict = CWServer.getInstance().getDictionary();
		cardAccountTypeDescrGR = dict.getCardAccountTypeGR(cardAccountType);
		cardAccountTypeDescrEN = dict.getCardAccountTypeEN(cardAccountType);
	}	
}
