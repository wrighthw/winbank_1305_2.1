package cws.impl.bal;


import cws.MessageResponder;
import cws.XmlTools;
import cws.impl.Statistics;
import cws.log.Logger;

public class BalancesResponder extends MessageResponder {

	public String processRequest(String message, String dataXML, String extraDataXML) throws Exception {	

//		Date date1 = new Date();

		int[] positions = new int[2];
		
		String participantID = XmlTools.parseTag(dataXML, "participantID", positions);	
		if (extraDataXML == null)
			extraDataXML = XmlTools.parseTag(dataXML, "cardNos", positions);
		String[] cardNos = parseStringArray(extraDataXML);
				 											
		BalancesData[] rows = getBalances(participantID, cardNos);
															
		String responseXML = generateResponseXML(rows);		
		
		
//		Date date2 = new Date();	
		
		String firstCardNo = "";
		if(cardNos.length>0)
		{
			firstCardNo = cardNos[0];
		}
			
		
//		Logger.logTimeAndRequestMessage("getBalances","firstCardNo:"+firstCardNo, date1, date2);
		
		return responseXML;		
	}

	protected BalancesData[] getBalances(String participantID, String[] cardNos) 
	throws Exception {
		
		BalancesData[] balances = null;
		try {
			long startTime = System.currentTimeMillis();		
			balances = BalancesImpl.execute(participantID, cardNos);
			long endTime = System.currentTimeMillis();
			Statistics.addRequest("getBalances", endTime - startTime);
		}
		catch (Exception ex) {
			Logger.logException(ex);
			balances = new BalancesData[0]; 
		}
											
		return balances; 
	}		

	protected String generateResponseXML(BalancesData[] rows) {	
		StringBuffer multiRefsXML = new StringBuffer();

		int count = rows.length;
		for (int i = 0; i < count; i++)
			appendArrayItem(multiRefsXML, i, rows[i]);
		
		return generateResponseXMLArray("getBalancesResponse", "getBalancesReturn",  
											"http://bal.impl.cws",
											"BalancesData", count, multiRefsXML.toString());			
	}

	protected void appendArrayItem(StringBuffer dataXML, int id, BalancesData data) {
		
		appendMultiRefStart(dataXML, id, "BalancesData");
								
		appendInteger(dataXML, "recordCount", data.getRecordCount());
		appendString(dataXML, "accountNo", data.getAccountNo());
		appendString(dataXML, "accountStatus", data.getAccountStatus());
		appendString(dataXML, "affinityCode", data.getAffinityCode());
		appendDecimal(dataXML, "availCashAdvLimit", data.getAvailCashAdvLimit());
		appendDecimal(dataXML, "availCashAdvLimitLocal", data.getAvailCashAdvLimitLocal());
		appendDecimal(dataXML, "availSpendLimit", data.getAvailSpendLimit());
		appendString(dataXML, "cardAccountType", data.getCardAccountType());
		appendDecimal(dataXML, "cashAdvLimit", data.getCashAdvLimit());
		appendDecimal(dataXML, "cashAdvLimitLocal", data.getAvailCashAdvLimitLocal());
		appendDecimal(dataXML, "creditLimit", data.getCreditLimit());
		appendString(dataXML, "currencyCode", data.getCurrencyCode());
		appendString(dataXML, "customerRel", data.getCustomerRel());
		appendDecimal(dataXML, "expiredPayAmount", data.getExpiredPayAmount());
		appendInteger(dataXML, "expiredPayCount", data.getExpiredPayCount());
		appendDecimal(dataXML, "lastPayAmount", data.getLastPayAmount());
		appendDate(dataXML, "lastPayDate", data.getLastPayDate());
		appendDecimal(dataXML, "minPayInterstBal", data.getMinPayInterstBal());
		appendDecimal(dataXML, "minPayLateCharge", data.getMinPayLateCharge());
		appendDecimal(dataXML, "minPayPrincipleBal", data.getMinPayPrincipleBal());
		appendString(dataXML, "participantID", data.getParticipantID());
		appendDate(dataXML, "payDueDate", data.getPayDueDate());
		appendDecimal(dataXML, "previousStmtBal", data.getPreviousStmtBal());
		appendString(dataXML, "primaryAdditional", data.getPrimaryAdditional());
		appendDecimal(dataXML, "spendLimit", data.getSpendLimit());
		appendString(dataXML, "cardAccountTypeDescrEN", encodeString(data.getCardAccountTypeDescrEN()));
		appendString(dataXML, "cardAccountTypeDescrGR", encodeString(data.getCardAccountTypeDescrGR()));
		appendDecimal(dataXML, "currBalanceNoAuth", data.getCurrBalanceNoAuth());
		appendDecimal(dataXML, "totalOutstandAuth", data.getTotalOutstandAuth());
										 
		appendMultiRefEnd(dataXML, "BalancesData");	
	}
}
