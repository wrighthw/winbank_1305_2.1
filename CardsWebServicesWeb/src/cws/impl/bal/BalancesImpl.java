package cws.impl.bal;

import java.math.BigDecimal;
import java.util.HashMap;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.QSYSObjectPathName;

import cws.impl.CWServer;
import cws.impl.MessageImpl;
import cws.impl.as400.AS400Server;
import cws.impl.as400.CharacterParameter;
import cws.impl.as400.DecimalParameter;
import cws.impl.as400.Parameter;

/*

Balance

Input

MSGNUM  CHAR        2    1 
CHPART  CHAR        3    Participant ID             
CHCARD  CHAR       19    Card Number                

Output
AMPART  CHAR        3    Participant ID    
AMACCT  CHAR       19    Account #  
AMCURC  CHAR        3    Currency Code                          
AMSTAT  CHAR        2    Account Status        
AMCREL  CHAR       10    Customer Relationship   
AMAFFC  CHAR        3    Affinity Code    
AMTYPE  CHAR        3    Card Account Type                  
AMLPYD  DECIMAL     7    Last Payment Date            
AMLPYA  DECIMAL    15    Last Payment Amount
AMEXPN  DECIMAL     3    # of Expired Payments
AMEXPA  DECIMAL    15    Amount of Expired Payments
AMMPYP  DECIMAL    15    Min Paym on Principle Bal   
AMMPYL  DECIMAL    15    Min Paym Late Charge   
AMMPYI  DECIMAL    15    Min Paym on Interest Bal 
AMPSBL  DECIMAL    15    Previous Statement Balance 
AMCPYD  DECIMAL     7    Payment Due Date     
CHPRIM  CHAR        1    Primary/Additional Card    
        DECIMAL    15    Credit Limit
        DECIMAL    15    Spending Limit
        DECIMAL    15    Available Spending Limit
        DECIMAL    15    Cash Advance Limit Local
        DECIMAL    15    Cash Advance Limit
        DECIMAL    15    Available Cash Advance Limit Local
        DECIMAL    15    Available Cash Advance Limit
        DECIMAL    15    Current Balance without Authorisations
        DECIMAL    15    Total Outstanding Authorisation
                 
Comments
In case of an AddOn (CHPRIM=�A�) the structure will be the same but only CHPRIM  and AMACCT will be populated. 
In AMACCT will stored the account in which the Card belongs.

Call RPG 

*/

public class BalancesImpl extends MessageImpl {

	public static BalancesData[] execute(
		String participantID, 	  // CHPART  CHAR        3    Participant ID                   
		String[] cardNos     	  // CHCARD  CHAR       19    Card Number                       
	) 
	throws Exception {

		/*
		participantID = "002";                   
		cardNos = new String[] {"4908450300596006", "4908450360327003", "4908450185271006"};                        		
		*/		

		CWServer cwsServer = CWServer.getInstance();		  				
		AS400Server server = cwsServer.getAS400();
		
		BalancesData[] data = null;
		AS400 con = null;
		try {
			con = server.openPGMConnection();
						
			server.runCommand(con, cwsServer.getSetting("CWS_CARDS_INIT"));
			
			String pgmLib = cwsServer.getSetting("CWS_CARDS_BAL_PGM_LIB");
			String pgm = cwsServer.getSetting("CWS_CARDS_BAL_PGM");
			String program = QSYSObjectPathName.toPath(pgmLib, pgm, "PGM");			
			Parameter[] input = new Parameter[] {
				new CharacterParameter("MSGNUM", "01", 2),				// (Message number)
				new CharacterParameter("CHPART", participantID, 3),		// Participant ID      
				new CharacterParameter("CHCARD", "", 19)				// Card Number   
			};
			Parameter[] output = new Parameter[] {								
				new CharacterParameter("AMPART", 3),   			// Participant ID    
				new CharacterParameter("AMACCT", 19),  			// Account #  
				new CharacterParameter("AMCURC", 3),      		// Currency Code                          
				new CharacterParameter("AMSTAT", 2),      		// Account Status          
				new CharacterParameter("AMCREL", 10),     		// Customer Relationship   
				new CharacterParameter("AMAFFC", 3),      		// Affinity Code    
				new CharacterParameter("AMTYPE", 3),      		// Card Account Type            				
				new DecimalParameter("AMLPYD", 7, 0),      		// Last Payment Date           
				new DecimalParameter("AMLPYA", 15, 0),     		// Last Payment Amount    
				new DecimalParameter("AMEXPN", 3, 0),      		// # of Expired Payments      
				new DecimalParameter("AMEXPA", 15, 0),     		// Amount of Expired Payments  
				new DecimalParameter("AMMPYP", 15, 0),     		// Min Paym on Principle Bal       
				new DecimalParameter("AMMPYL", 15, 0),     		// Min Paym Late Charge                
				new DecimalParameter("AMMPYI", 15, 0),     		// Min Paym on Interest Bal 
				new DecimalParameter("AMPSBL", 15, 0),     		// Previous Statement Balance				
				new DecimalParameter("AMCPYD", 7, 0),     	    // Payment Due Date     				
				new CharacterParameter("CHPRIM", 1),      		// Primary/Additional Card 
				new DecimalParameter("_CRDLIM", 15, 0),    		// Credit Limit
				new DecimalParameter("_SPDLIM", 15, 0),    		// Spending Limit
				new DecimalParameter("_ASPLIM", 15, 0),    		// Available Spending Limit
				new DecimalParameter("_CALIML", 15, 0),    		// Cash Advance Limit Local
				new DecimalParameter("_CADLIM", 15, 0),    		// Cash Advance Limit
				new DecimalParameter("_ACLIML", 15, 0),    		// Available Cash Advance Limit Local
				new DecimalParameter("_ACALIM", 15, 0),			// Available Cash Advance Limit		
				new DecimalParameter("_CBALWA", 15, 0),			// Current Balance without Authorisations
				new DecimalParameter("_TOUTAT", 15, 0)			// Total Outstanding Authorisation
			};
			
			int count = cardNos.length;
			data = new BalancesData[count];
			BalancesData entry;
			HashMap result; 			
			for (int c = 0; c < count; c++) {
				input[2] = new CharacterParameter("CHCARD", cardNos[c], 19);							
				result = server.runProgram(con, program, input, output); 
				
				entry = new BalancesData();				
				
				entry.recordCount 				= count; 
				entry.participantID 			= handleNull((String) result.get("AMPART"));    
				entry.accountNo 				= handleNull((String) result.get("AMACCT"));  
				entry.currencyCode 				= handleNull((String) result.get("AMCURC"));                          
				entry.accountStatus 			= handleNull((String) result.get("AMSTAT"));        
				entry.customerRel 				= handleNull((String) result.get("AMCREL"));   
				entry.affinityCode 				= handleNull((String) result.get("AMAFFC"));    
				entry.cardAccountType 			= handleNull((String) result.get("AMTYPE"));
				                  
				entry.lastPayDate 				= (handleNull((BigDecimal) result.get("AMLPYD"))).intValue();            
				entry.lastPayAmount 			= handleNull((BigDecimal) result.get("AMLPYA"));     
				entry.expiredPayCount 			= (handleNull((BigDecimal) result.get("AMEXPN"))).intValue();          
				entry.expiredPayAmount 			= handleNull((BigDecimal) result.get("AMEXPA")); 
				entry.minPayPrincipleBal 		= handleNull((BigDecimal) result.get("AMMPYP"));         
				entry.minPayLateCharge 			= handleNull((BigDecimal) result.get("AMMPYL"));              
				entry.minPayInterstBal 			= handleNull((BigDecimal) result.get("AMMPYI"));   
				entry.previousStmtBal 			= handleNull((BigDecimal) result.get("AMPSBL"));   
				entry.payDueDate 				= (handleNull((BigDecimal) result.get("AMCPYD"))).intValue();
				     
				entry.primaryAdditional 		= handleNull((String) result.get("CHPRIM"));
				entry.creditLimit 				= handleNull((BigDecimal) result.get("_CRDLIM"));
				entry.spendLimit 				= handleNull((BigDecimal) result.get("_SPDLIM"));
				entry.availSpendLimit 			= handleNull((BigDecimal) result.get("_ASPLIM"));
				entry.cashAdvLimitLocal 		= handleNull((BigDecimal) result.get("_CALIML"));
				entry.cashAdvLimit 				= handleNull((BigDecimal) result.get("_CADLIM"));
				entry.availCashAdvLimitLocal 	= handleNull((BigDecimal) result.get("_ACLIML"));
				entry.availCashAdvLimit 		= handleNull((BigDecimal) result.get("_ACALIM"));

				entry.currBalanceNoAuth			= handleNull((BigDecimal) result.get("_CBALWA"));	// Current Balance without Authorisations
				entry.totalOutstandAuth			= handleNull((BigDecimal) result.get("_TOUTAT"));	// Total Outstanding Authorisation
				
				entry.transform();	
				
				data[c] = entry;
			}
		}
		finally {
			if (con != null)
				server.closePGMConnection(con);
		}

		return data;
	}
}
