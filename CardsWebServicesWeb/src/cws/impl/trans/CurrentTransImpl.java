package cws.impl.trans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import cws.impl.CWServer;
import cws.impl.MessageImpl;
import cws.impl.ResultSetIterator;

/*

Current Transactions (Unbilled)

Input

MSGNUM  NUMERIC     2    3 
CTPART  CHAR        3    Participant Id               
CTACCT  CHAR       19    Account #                    
CTABLC  CHAR        3    Account Billing Curr Code   

Output

CTPART  CHAR        3    Participant Id               
CTACCT  CHAR       19    Account #  
CTABLC  CHAR        3    Account Billing Curr Code   
CTPRDT  DECIMAL     7    Process Date                 
CTTRDT  DECIMAL     7    Transaction Date             
CTCARD  CHAR       19    Card #                       
CTREFN  CHAR       12    Reference #                  
CTAPRN  CHAR        6    Approval #                   
CTRVSL  NUMERIC     1    Reversal Indicator  
CTSTMF  CHAR        1    Remove From Statement             
CTTRTY  CHAR        2    Transaction Type                  
CTDBCR  CHAR        1    Debit/Credit Flag                 
CTTRCC  CHAR        3    Transaction Curr Code             
CTTRNA  DECIMAL    15    Transaction Amount                
CTABLA  DECIMAL    15    Account Billing Amount            
CTORGN  CHAR        3    Transaction Origin Code            
CTCADS  CHAR       40    Card Acceptor Description          
CTPDS1  CHAR       40    Posting Description 1              
CTPDS2  CHAR       40    Posting Description 2              

File : CTRNCU0P                                                                                                      
 
*/

public class CurrentTransImpl extends MessageImpl {

	public static CurrentTransData[] execute(
		String participantID, 		// CTPART  CHAR        3    Participant ID                   
		String accountNo,     		// CTACCT  CHAR       19    Account #                        
		String cardNo,     			// CTCARD  CHAR       19    Card #
		String billCurrencyCode, 	// CTABLC  CHAR        3    Account Billing Curr Code
		boolean sortByCardNo,
		int start,            		// 1-based
		int count		
	) 
	throws SQLException {
		
		return execute(participantID, accountNo, cardNo, billCurrencyCode, null, null, sortByCardNo, start, count);		
	}
	
	public static CurrentTransData[] execute(
		String participantID, 		// CTPART  CHAR        3    Participant ID                   
		String accountNo,     		// CTACCT  CHAR       19    Account #
		String cardNo,     			// CTCARD  CHAR       19    Card #                        
		String billCurrencyCode, 	// CTABLC  CHAR        3    Account Billing Curr Code
		Date dateFrom,			  			  
		Date dateTo,		
		boolean sortByCardNo,
		int start,            		// 1-based
		int count		
	) 
	throws SQLException {

		/*
		participantID = "002";                   
		accountNo = "4908450208887002";  
		cardNo = "";                      
		billCurrencyCode = "978";
		sortByCardNo = false;
		start = 1;
		count = 5;
		*/		

		CurrentTransData[] data = new CurrentTransData[count]; 
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs  = null;
		int index = 0;
		try {
			con = CWServer.getInstance().getAS400().getDBConnection();

			/* count rows */
			
			String sql = "SELECT COUNT(*)"
				+ " FROM " + getCardsLibrary() + ".CTRNCU0P"
				+ " WHERE CTPART = ? AND CTACCT = ? AND CTABLC = ?";
			if (dateFrom != null && dateTo != null)
				sql += " AND CTPRDT >= ? AND CTPRDT <= ?";
			if (cardNo != null)
				sql += " AND CTCARD = ?"; 							
									
			ps = con.prepareStatement(sql);	
			int pos = 1;
			ps.setString(pos, participantID); pos++;
			ps.setString(pos, accountNo); pos++;
			ps.setString(pos, billCurrencyCode); pos++;
			if (dateFrom != null && dateTo != null) {
				ps.setInt(pos, encodeDate(dateFrom)); pos++;
				ps.setInt(pos, encodeDate(dateTo)); pos++;
			}
			if (cardNo != null) {			
				ps.setString(pos, cardNo); pos++;
			}
							
			rs = ps.executeQuery();
			rs.next();
			int recordCount = rs.getInt(1);			
			
			/* get actual data */
			
			sql = "SELECT"				
				+ " CTPART, CTACCT, CTABLC, CTPRDT, CTTRDT, CTCARD, CTREFN, CTAPRN,"                   
				+ " CTRVSL, CTSTMF, CTTRTY, CTDBCR, CTTRCC, CTTRNA, CTABLA, CTORGN,"            
				+ " CTCADS, CTPDS1, CTPDS2"							 
				+ " FROM " + getCardsLibrary() + ".CTRNCU0P"
				+ " WHERE CTPART = ? AND CTACCT = ? AND CTABLC = ?";
			if (dateFrom != null && dateTo != null)
				sql += " AND CTPRDT >= ? AND CTPRDT <= ?";
			if (cardNo != null)
				sql += " AND CTCARD = ?"; 															
			sql += " ORDER BY";
			if (sortByCardNo)
				sql += " CTCARD ASC,";
			sql += " CTPRDT DESC, CTTRDT DESC";
			
			ps = con.prepareStatement(sql);			
			pos = 1;
			ps.setString(pos, participantID); pos++;
			ps.setString(pos, accountNo); pos++;
			ps.setString(pos, billCurrencyCode); pos++;
			if (dateFrom != null && dateTo != null) {
				ps.setInt(pos, encodeDate(dateFrom)); pos++;
				ps.setInt(pos, encodeDate(dateTo)); pos++;
			}
			if (cardNo != null) {			
				ps.setString(pos, cardNo); pos++;
			}
			
			rs = ps.executeQuery();			
			
			CurrentTransData entry;			
			ResultSetIterator iter = new ResultSetIterator(rs, start, count);
			if (iter.hasRows()) {
				do {
					entry = new CurrentTransData();
					
					entry.recordCount 		= recordCount;
					entry.participantID 	= handleNull(rs.getString("CTPART"));               
					entry.accountNo 		= handleNull(rs.getString("CTACCT"));  
					entry.billCurrencyCode 	= handleNull(rs.getString("CTABLC"));   
					entry.processDate 		= rs.getInt("CTPRDT");                 
					entry.transDate 		= rs.getInt("CTTRDT");             
					entry.cardNo 			= handleNull(rs.getString("CTCARD"));                       
					entry.referenceNo 		= handleNull(rs.getString("CTREFN"));                  
					entry.approvalNo 		= handleNull(rs.getString("CTAPRN"));                   
					entry.reversalInd 		= rs.getInt("CTRVSL");  
					entry.removeFromStmt 	= handleNull(rs.getString("CTSTMF"));             
					entry.transType 		= handleNull(rs.getString("CTTRTY"));       
					entry.debitCredit 		= handleNull(rs.getString("CTDBCR"));                 
					entry.transCurrencyCode = handleNull(rs.getString("CTTRCC"));             
					entry.transAmount		= handleNull(rs.getBigDecimal("CTTRNA"));                
					entry.accBillAmount 	= handleNull(rs.getBigDecimal("CTABLA"));            
					entry.transOrgCode 		= handleNull(rs.getString("CTORGN"));
					entry.cardAcceptDescr 	= handleNull(rs.getString("CTCADS"));          
					entry.postingDescr1		= handleNull(rs.getString("CTPDS1"));              
					entry.postingDescr2		= handleNull(rs.getString("CTPDS2"));
						   
					entry.transform();
											              									
					data[index] = entry;
					index++;
				} while (iter.next());
			}	 			
		}
		finally {
			closeObjects(con, ps, rs);
		}
		
		CurrentTransData[] trimmedData = new CurrentTransData[index];		
		System.arraycopy(data, 0, trimmedData, 0, index);				  
		return trimmedData;
	}
}
