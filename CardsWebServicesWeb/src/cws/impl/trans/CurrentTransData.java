package cws.impl.trans;

import java.math.BigDecimal;
import java.util.Date;

import cws.impl.CWServer;
import cws.impl.MessageImpl;
import cws.impl.ParameterDictionary;

public class CurrentTransData {
	int recordCount;
	String participantID;		// CTPART  CHAR        3    Participant Id               
	String accountNo;			// CTACCT  CHAR       19    Account #  
	String billCurrencyCode;	// CTABLC  CHAR        3    Account Billing Curr Code   
	int processDate;			// CTPRDT  DECIMAL     7    Process Date
		Date processDateMsg;
	int transDate;				// CTTRDT  DECIMAL     7    Transaction Date
		Date transDateMsg;	             
	String cardNo;				// CTCARD  CHAR       19    Card #                       
	String referenceNo;			// CTREFN  CHAR       12    Reference #                  
	String approvalNo;			// CTAPRN  CHAR        6    Approval #                   
	int reversalInd;			// CTRVSL  NUMERIC     1    Reversal Indicator  
	String removeFromStmt;		// CTSTMF  CHAR        1    Remove From Statement             
	String transType;			// CTTRTY  CHAR        2    Transaction Type
	String transTypeDescrGR; 
	String transTypeDescrEN;                   
	String debitCredit;			// CTDBCR  CHAR        1    Debit/Credit Flag                 
	String transCurrencyCode;	// CTTRCC  CHAR        3    Transaction Curr Code             
	BigDecimal transAmount;		// CTTRNA  DECIMAL    15    Transaction Amount
		BigDecimal transAmountMsg;                
	BigDecimal accBillAmount;	// CTABLA  DECIMAL    15    Account Billing Amount
		BigDecimal accBillAmountMsg;            
	String transOrgCode;		// CTORGN  CHAR        3    Transaction Origin Code            
	String cardAcceptDescr;		// CTCADS  CHAR       40    Card Acceptor Description          
	String postingDescr1;		// CTPDS1  CHAR       40    Posting Description 1              
	String postingDescr2;		// CTPDS2  CHAR       40    Posting Description 2      
	
	public CurrentTransData() {        
	}
	
	public BigDecimal getAccBillAmount() {
		return accBillAmountMsg;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public String getApprovalNo() {
		return approvalNo;
	}

	public String getBillCurrencyCode() {
		return billCurrencyCode;
	}

	public String getCardAcceptDescr() {
		return cardAcceptDescr;
	}

	public String getCardNo() {
		return cardNo;
	}

	public String getDebitCredit() {
		return debitCredit;
	}

	public String getParticipantID() {
		return participantID;
	}

	public String getPostingDescr1() {
		return postingDescr1;
	}

	public String getPostingDescr2() {
		return postingDescr2;
	}

	public Date getProcessDate() {
		return processDateMsg;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public String getRemoveFromStmt() {
		return removeFromStmt;
	}

	public int getReversalInd() {
		return reversalInd;
	}

	public BigDecimal getTransAmount() {
		return transAmountMsg;
	}

	public String getTransCurrencyCode() {
		return transCurrencyCode;
	}

	public Date getTransDate() {
		return transDateMsg;
	}

	public String getTransOrgCode() {
		return transOrgCode;
	}

	public String getTransType() {
		return transType;
	}

	public int getRecordCount() {
		return recordCount;
	}

	public String getTransTypeDescrEN() {
		return transTypeDescrEN;
	}

	public String getTransTypeDescrGR() {
		return transTypeDescrGR;
	}

	public void transform() {		
		processDateMsg = MessageImpl.decodeDate(processDate);
		transDateMsg = MessageImpl.decodeDate(transDate);
		transAmountMsg = MessageImpl.decodeAmount(transAmount, MessageImpl.getDecimalPlaces());
		accBillAmountMsg = MessageImpl.decodeAmount(accBillAmount, MessageImpl.getDecimalPlaces());
		
		ParameterDictionary dict = CWServer.getInstance().getDictionary();
		transTypeDescrGR = dict.getTransactionTypeGR(transType); 
		transTypeDescrEN = dict.getTransactionTypeEN(transType); 				
	}	
}
