package cws.impl.trans;

import cws.MessageResponder;
import cws.XmlTools;
import cws.impl.Statistics;
import cws.log.Logger;

public class CurrentTransResponder extends MessageResponder {

	public String processRequest(String message, String dataXML, String extraDataXML) throws Exception {	
		
//		Date date1 = new Date();

		int[] positions = new int[2];
		
		String participantID = XmlTools.parseTag(dataXML, "participantID", positions);					
		String accountNo = XmlTools.parseTag(dataXML, "accountNo", positions); 
		String cardNo = XmlTools.parseTag(dataXML, "cardNo", positions);
		String billCurrencyCode = XmlTools.parseTag(dataXML, "billCurrencyCode", positions); 
		boolean sortByCardNo = Boolean.valueOf(XmlTools.parseTag(dataXML, "sortByCardNo", positions)).booleanValue();
		int start = Integer.parseInt(XmlTools.parseTag(dataXML, "start", positions));
		int count = Integer.parseInt(XmlTools.parseTag(dataXML, "count", positions));																									
						 													
		CurrentTransData[] rows = getCurrentTransactions(participantID, accountNo, cardNo, billCurrencyCode, 
															sortByCardNo, start, count);
																																
		String responseXML = generateResponseXML(rows);
		
//		Date date2 = new Date();
//		
//		Logger.logTimeAndRequestMessage("getCurrentTransactions","cardNo:"+cardNo, date1, date2);
				
		return responseXML;		
	}

	protected CurrentTransData[] getCurrentTransactions(String participantID, String accountNo, String cardNo, 
															String billCurrencyCode, 
															boolean sortByCardNo,
															int start, int count) 
	throws Exception {
		
		CurrentTransData[] transactions = null;
		try {
			long startTime = System.currentTimeMillis();
			if (cardNo != null && (cardNo.equals("") || cardNo.equals(" ")))
				cardNo = null;
			transactions = CurrentTransImpl.execute(participantID, accountNo, cardNo, billCurrencyCode,
													sortByCardNo, start, count);
			long endTime = System.currentTimeMillis();
			Statistics.addRequest("getCurrentTransactions", endTime - startTime);		
		}
		catch (Exception ex) {
			Logger.logException(ex);
			transactions = new CurrentTransData[0];
		}
											
		return transactions; 
	}

	protected String generateResponseXML(CurrentTransData[] rows) {	
		StringBuffer multiRefsXML = new StringBuffer();

		int count = rows.length;
		for (int i = 0; i < count; i++)
			appendArrayItem(multiRefsXML, i, rows[i]);
		
		return generateResponseXMLArray("getCurrentTransactionsResponse", "getCurrentTransactionsReturn",  
											"http://trans.impl.cws",
											"CurrentTransData", count, multiRefsXML.toString());			
	}

	protected void appendArrayItem(StringBuffer dataXML, int id, CurrentTransData data) {
		
		appendMultiRefStart(dataXML, id, "CurrentTransData");
								
		appendDecimal(dataXML, "accBillAmount", data.getAccBillAmount());
	 	appendString(dataXML, "accountNo", data.getAccountNo());
		appendString(dataXML, "approvalNo", data.getApprovalNo());
		appendString(dataXML, "billCurrencyCode", data.getBillCurrencyCode());
		appendString(dataXML, "cardAcceptDescr", encodeString(data.getCardAcceptDescr()));
		appendString(dataXML, "cardNo", data.getCardNo());
		appendString(dataXML, "debitCredit", data.getDebitCredit());
		appendString(dataXML, "participantID", data.getParticipantID());
		appendString(dataXML, "postingDescr1", encodeString(data.getPostingDescr1()));
		appendString(dataXML, "postingDescr2", encodeString(data.getPostingDescr2()));
		appendDate(dataXML, "processDate", data.getProcessDate());
		appendString(dataXML, "referenceNo", data.getReferenceNo());
		appendString(dataXML, "removeFromStmt", data.getRemoveFromStmt());
		appendInteger(dataXML, "reversalInd", data.getReversalInd());
		appendDecimal(dataXML, "transAmount", data.getTransAmount());
		appendString(dataXML, "transCurrencyCode", data.getTransCurrencyCode());
		appendDate(dataXML, "transDate", data.getTransDate());
		appendString(dataXML, "transOrgCode", data.getTransOrgCode());
		appendString(dataXML, "transType", data.getTransType());
		appendInteger(dataXML, "recordCount", data.getRecordCount());
		appendString(dataXML, "transTypeDescrEN", encodeString(data.getTransTypeDescrEN()));
		appendString(dataXML, "transTypeDescrGR", encodeString(data.getTransTypeDescrGR()));										 
			 
		appendMultiRefEnd(dataXML, "CurrentTransData");	
	}
}
