package cws.impl;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import cws.log.Logger;

public class Statistics {
	static final String days[] = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};

	static GregorianCalendar startCalendar;
	static long countRequests;
	static long maxResponseTime;
	static long minResponseTime;
	static long totalResponseTime;
	
	static long[][] callFrequency;
	static boolean newWeek; 
	
	public static synchronized void init() {
		startCalendar = new GregorianCalendar();
		countRequests = 0;
		maxResponseTime = 0;
		minResponseTime = Long.MAX_VALUE;
		totalResponseTime = 0;
		callFrequency = new long[365][24];
		newWeek = false;
	}
	
	public static synchronized void addRequest(String name, long responseTime) {
		try {
			if (name.equals("getBalances")) {		
				GregorianCalendar calendar = new GregorianCalendar();
				int dayIndex = calendar.get(Calendar.DAY_OF_YEAR) - 1;
				int hourIndex = calendar.get(Calendar.HOUR_OF_DAY);				
				callFrequency[dayIndex][hourIndex]++;
			}
			
			countRequests++;
			if (responseTime > maxResponseTime)
				maxResponseTime = responseTime; 
			if (responseTime < minResponseTime)
				minResponseTime = responseTime; 
			totalResponseTime += responseTime;			
		}
		catch (Exception ex) {
			Logger.logException(ex); 		
		}		
	}

	public static synchronized String getStatisticsHTML(int week) {
		StringBuffer html = new StringBuffer();
		DecimalFormat numFormatter = new DecimalFormat("0.00");
		SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy hh:mm");
				
		html.append("<br>");	
		double average = (countRequests > 0) ? ((double) totalResponseTime/countRequests/1000.0) : 0.0;
		double minimum = (minResponseTime < Long.MAX_VALUE) ? (minResponseTime/1000.0) : 0.0;
		html.append("<table border=1>")
			.append("<tr><td>Up since&nbsp;&nbsp;</td><td>").append(dateFormatter.format(startCalendar.getTime())).append("</td></tr>")
			.append("<tr><td>Total Requests&nbsp;&nbsp;</td><td>").append(countRequests).append("</td></tr>")
			.append("<tr><td>Average Response Time&nbsp;&nbsp;</td><td>").append(numFormatter.format(average)).append(" sec</td></tr>")
			.append("<tr><td>Max. Response Time&nbsp;&nbsp;</td><td>").append(numFormatter.format(maxResponseTime/1000.0)).append(" sec</td></tr>")
			.append("<tr><td>Min. Response Time&nbsp;&nbsp;</td><td>").append(numFormatter.format(minimum)).append(" sec</td></tr>")
			.append("</table>");
					
		html.append("<br>");
		GregorianCalendar weekCalendar = new GregorianCalendar();
		if (week > 0)
			weekCalendar.set(Calendar.WEEK_OF_YEAR, week);
		int dayIndex = weekCalendar.get(Calendar.DAY_OF_YEAR)-1;		
		int h, d;
		html.append("<table border=1>");
		 
		html.append("<tr><td>Hour</td>");
		for (d = 0; d < 7; d++)				
			html.append("<td>").append(days[d]).append("</td>");
				
		for (h = 0; h < 24; h++) {
			html.append("<tr><td>").append(h).append("</td>");
			for (d = 0; d < 7; d++) {
				html.append("<td>").append(callFrequency[dayIndex+d][h]).append("</td>");
			}			
			html.append("</tr>");
		}
		html.append("</table>");
		
		return html.toString();
	}
}
