package cws.impl.as400;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import javax.sql.DataSource;
import javax.sql.PooledConnection;
import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400ConnectionPool;
import com.ibm.as400.access.AS400JDBCConnectionPoolDataSource;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.CommandCall;
import com.ibm.as400.access.ConnectionPoolException;
import com.ibm.as400.access.ProgramCall;
import com.ibm.as400.access.ProgramParameter;

import cws.impl.CWServer;

public class AS400Server {
	protected static int DECIMAL_SIZE = 15;
		
	String serverName;
	String user;
	String password;
	AS400ConnectionPool pool; 
	DataSource dataSource;
	 
	public AS400Server() { 		
		
		CWServer cwsServer = CWServer.getInstance();		  
		this.serverName = cwsServer.getSetting("CWS_CARDS_SERVER");
		this.user = cwsServer.getSetting("CWS_CARDS_USER");
		this.password = cwsServer.getSetting("CWS_CARDS_PASSWORD");				
		
		this.pool = new AS400ConnectionPool();		
						
		this.dataSource = new AS400JDBCConnectionPoolDataSource(serverName);
		((AS400JDBCConnectionPoolDataSource) dataSource).setUser(user);
		((AS400JDBCConnectionPoolDataSource) dataSource).setPassword(password);
	}
	
	public synchronized AS400 openPGMConnection() throws ConnectionPoolException {		
		return pool.getConnection(serverName, user, password, AS400.COMMAND);
	}
	
	public synchronized void closePGMConnection(AS400 connection) {
		pool.returnConnectionToPool(connection);
	}
	
	public synchronized Connection getDBConnection() throws SQLException {
		PooledConnection pc = ((AS400JDBCConnectionPoolDataSource) dataSource).getPooledConnection();
		return pc.getConnection();
	}
		
	public void runCommand(AS400 connection, String command) 
	throws Exception {
		
		CommandCall cc = new CommandCall(connection, command);		
		
		if (cc.run() != true) {
			AS400Message[] messageList = cc.getMessageList();
			int count = messageList.length;
			StringBuffer msg = new StringBuffer();
			for (int m = 0; m < count; m++) {
				msg.append(messageList[m].getText());
				msg.append("\n");
			}
			throw new Exception(msg.toString());
		}		 	
	}
	
	public HashMap runProgram(AS400 connection, String program, 
								Parameter[] input, Parameter[] output) 
	throws Exception {
		
		ProgramParameter[] paramList = new ProgramParameter[input.length + output.length];
		
		int countIn = input.length;		
		for (int ip = 0; ip < countIn; ip++)
			 paramList[ip] = input[ip].getInputParameter(connection);		
		
		int countOut = output.length;
		int op;		
		for (op = 0; op < countOut; op++)
			paramList[countIn + op] = output[op].getOutputParameter(connection);
								
		ProgramCall pgm = new ProgramCall(connection, program, paramList);		
		
		if (pgm.run() != true) {
			AS400Message[] messageList = pgm.getMessageList();
			int count = messageList.length;
			StringBuffer msg = new StringBuffer();
			for (int m = 0; m < count; m++) {
				msg.append(messageList[m].getText());
				msg.append("\n");
			}
			throw new Exception(msg.toString());
		}

		
		HashMap results = new HashMap();
		String name;
		Object value;
		for (op = 0; op < countOut; op++) {
			name = output[op].getName();
			value = output[op].getOutputValue(connection, paramList[countIn + op]);
			results.put(name, value); 
		}			 		
				
		return results;
	}	
}
