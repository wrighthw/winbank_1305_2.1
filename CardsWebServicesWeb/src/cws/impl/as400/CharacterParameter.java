package cws.impl.as400;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

public class CharacterParameter extends Parameter {	
	String value;
	int size;	

	public CharacterParameter(String name, String value, int size) {
		super(name);
		
		this.value = value;
		this.size = size;		 
	}

	public CharacterParameter(String name, Date dateValue) {
		super(name);
		
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(dateValue);		
		
		DecimalFormat formatter = new DecimalFormat("000");		
		this.value = formatter.format(cal.get(Calendar.YEAR) - 1900);
		formatter = new DecimalFormat("00");
		this.value += formatter.format(cal.get(Calendar.MONTH) + 1); 
		this.value += formatter.format(cal.get(Calendar.DATE));
		
		this.size = 7;		 
	}
	
	public ProgramParameter getInputParameter(AS400 connection) {
		AS400Text textConverter = new AS400Text(size, connection);		
		byte[] data = textConverter.toBytes(value);
		ProgramParameter parameter = new ProgramParameter(data);		
		return parameter;
	}

	public ProgramParameter getOutputParameter(AS400 connection) {
		ProgramParameter parameter = new ProgramParameter(size);			 	
		return parameter;
	}

	public CharacterParameter(String name, int size) {
		super(name);
		
		this.size = size;		 		
	}

	public Object getOutputValue(AS400 connection, ProgramParameter param) {
		AS400Text textConverter = new AS400Text(size, connection);
		byte[] data = param.getOutputData();		
		
		if (isEmpty(data))
			this.value = "";
		else
			this.value = (String) textConverter.toObject(data);
			 
		return value;
	}
}

