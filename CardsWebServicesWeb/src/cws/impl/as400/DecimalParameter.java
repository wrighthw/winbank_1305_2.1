package cws.impl.as400;

import java.math.BigDecimal;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.ProgramParameter;

public class DecimalParameter extends Parameter {
	BigDecimal value;
	int size;
	int decimals;

	public DecimalParameter(String name, BigDecimal value, int size, int decimals) {
		super(name);
		
		this.value = value;
		this.size = size;
		this.decimals = decimals; 
	}
	
	public ProgramParameter getInputParameter(AS400 connection) {
		AS400PackedDecimal decimalConverter = new AS400PackedDecimal(size, decimals);		
		byte[] data = decimalConverter.toBytes(value);
		ProgramParameter parameter = new ProgramParameter(data);		
		return parameter;
	}

	public ProgramParameter getOutputParameter(AS400 connection) {
		ProgramParameter parameter = new ProgramParameter(size);		
		return parameter;
	}

	public DecimalParameter(String name, int size, int decimals) {
		super(name);
		
		this.size = size;
		this.decimals = decimals; 			
	}

	public Object getOutputValue(AS400 connection, ProgramParameter param) {
		AS400PackedDecimal decimalConverter = new AS400PackedDecimal(size, decimals);		
		byte[] data = param.getOutputData();
		
		if (isEmpty(data))
			this.value = new BigDecimal(0);
		else {					
			this.value = (BigDecimal) decimalConverter.toObject(data);
			if (value.signum() == 0)
				this.value = new BigDecimal(0);
		}
			 
		return value;
	}
}

