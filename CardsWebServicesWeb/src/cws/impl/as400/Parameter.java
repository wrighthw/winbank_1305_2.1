package cws.impl.as400;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.ProgramParameter;

public abstract class Parameter {
	String name;
	
	public Parameter(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public abstract ProgramParameter getInputParameter(AS400 connection);
	public abstract ProgramParameter getOutputParameter(AS400 connection);
	public abstract Object getOutputValue(AS400 connection, ProgramParameter param);
	
	protected boolean isEmpty(byte[] data) {	
		int count = data.length;
		boolean isEmpty = true;
		for (int b = 0; b < count; b++) {		
			if (data[b] != 0) {
				isEmpty = false;
				break;
			}			
		}
		return isEmpty; 
	}
}

