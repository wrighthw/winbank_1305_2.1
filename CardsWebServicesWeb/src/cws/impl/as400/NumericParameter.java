package cws.impl.as400;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Bin4;
import com.ibm.as400.access.ProgramParameter;

public class NumericParameter extends Parameter {	
	Integer value;

	public NumericParameter(String name, int value) {
		super(name);
				
		this.value = new Integer(value); 
	}

	public NumericParameter(String name, Integer value) {
		super(name);
		
		this.value = value; 
	}
	
	public ProgramParameter getInputParameter(AS400 connection) {
		AS400Bin4 intConverter = new AS400Bin4();
		byte[] data = intConverter.toBytes(value);
		ProgramParameter parameter = new ProgramParameter(data);		
		return parameter; 
	}

	public ProgramParameter getOutputParameter(AS400 connection) {
		ProgramParameter parameter = new ProgramParameter(4);		
		return parameter; 
	}

	public NumericParameter(String name) {
		super(name);		
	}

	public Object getOutputValue(AS400 connection, ProgramParameter param) {
		AS400Bin4 intConverter = new AS400Bin4();
		byte[] data = param.getOutputData();	
		
		if (isEmpty(data))
			this.value = new Integer(0);
		else
			this.value = (Integer) intConverter.toObject(data);
		return value;
	}
}
