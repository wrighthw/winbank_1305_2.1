package cws.impl;

import java.util.Properties;

import cws.impl.as400.AS400Server;
import cws.log.Logger;

public class CWServer {
	protected static boolean IS_LIVE = false;
//	protected static boolean IS_LIVE = true;
		
	protected static CWServer server = null;	
	
	public static CWServer getInstance() {
		if (server == null)
			startUp();
			
		return server;
	}
	
	public static void startUp() {
		server = new CWServer(IS_LIVE);
		Statistics.init();
		
		if (server.start()) 		
			Logger.logInfo("CWS", "CWS started.");
	}

	public static void shutDown() {
		server.stop(); 
	}

	AS400Server as400Server;
	ParameterDictionary dictionary;
	Properties settings;
	boolean isLive;

	protected CWServer(boolean isLive) {
		this.isLive = isLive;
		
		settings = new Properties();
		if (isLive) {
			// login									
			settings.setProperty("CWS_CARDS_SERVER", "GR400PR");
			//settings.setProperty("CWS_CARDS_SERVER", "ABCARDS");
			settings.setProperty("CWS_CARDS_USER", "ABCWEBSRV");
			settings.setProperty("CWS_CARDS_PASSWORD", "SRVWEBABC");
			// SQL library
			settings.setProperty("CWS_CARDS_LIBRARY", "ASID154401");
			// Environment setup command
			settings.setProperty("CWS_CARDS_INIT", "EN");//"EN"
			// Get-Balances program
			settings.setProperty("CWS_CARDS_BAL_PGM_LIB", "CARDPROD");
			settings.setProperty("CWS_CARDS_BAL_PGM", "RGET601C");
			// Verify-Credentials program
			settings.setProperty("CWS_CARDS_CRED_PGM_LIB", "CARDPROD");
			settings.setProperty("CWS_CARDS_CRED_PGM", "RGET660C");
			

			//pira to apo katw			

			
			
			
		}
		else {		
			// login									
			settings.setProperty("CWS_CARDS_SERVER", "ABCDEV");
			settings.setProperty("CWS_CARDS_USER", "ABCWEBSRV");
			settings.setProperty("CWS_CARDS_PASSWORD", "SRVWEBABC");
			// SQL library			
			settings.setProperty("CWS_CARDS_LIBRARY", "ASID154408");
			// Environment setup command			
			settings.setProperty("CWS_CARDS_INIT", "WEB");
			// Get-Balances program			
			settings.setProperty("CWS_CARDS_BAL_PGM_LIB", "CARDPB");
			settings.setProperty("CWS_CARDS_BAL_PGM", "RGET601C");
			// Verify-Credentials program			
			settings.setProperty("CWS_CARDS_CRED_PGM_LIB", "CARDPB");
			settings.setProperty("CWS_CARDS_CRED_PGM", "RGET660C");			
		}
	}

	protected boolean start() {
		boolean success = true;
		
		try {		
			this.as400Server = new AS400Server();			
		}
		catch (Exception ex) {
			success = false;
			Logger.logException(ex);
		}

		try {					
			this.dictionary = new ParameterDictionary();
		}
		catch (Exception ex) {
			success = false;
			Logger.logException(ex);
		}
		
		return success;
	}
	
	protected void stop() {
	}

	public AS400Server getAS400() {
		return as400Server;
	}
	
	public ParameterDictionary getDictionary() {
		return dictionary;
	}
	
	public boolean isLive() {
		return isLive;
	}
	
	public String getSetting(String name) {
		return settings.getProperty(name);
	}
}
