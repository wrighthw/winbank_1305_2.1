package cws.impl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


public class MessageImpl {
			
	protected static String getCardsLibrary() {	
		return CWServer.getInstance().getSetting("CWS_CARDS_LIBRARY");
	}
	
	protected static void closeObjects(Connection con, PreparedStatement ps, ResultSet rs) {
		
		try {							
			if (rs != null)
				rs.close();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}

		try {							
			if (ps != null)
				ps.close();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	
		try {							
			if (con != null)
				con.close();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public static int encodeDate(Date date) {		
		
		if (date == null)
			return 0;
		
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		
		int year = calendar.get(Calendar.YEAR) - 2000 + 100;
		int month = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DATE);
		
		DecimalFormat formatter = new DecimalFormat("00"); 
		String encoded = year + formatter.format(month) + formatter.format(day);   
		
		return Integer.valueOf(encoded).intValue();
	}

	public static int encodeTime(Date date) {		
		
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		
		int hours = calendar.get(Calendar.HOUR);
		int minutes = calendar.get(Calendar.MINUTE);
		int seconds = calendar.get(Calendar.SECOND);
		
		DecimalFormat formatter = new DecimalFormat("00"); 
		String encoded = formatter.format(hours) + formatter.format(minutes) + formatter.format(seconds);   
		
		return Integer.valueOf(encoded).intValue();
	}
	
	public static Date decodeDate(int encodedDate) {
		return decodeDateTime(encodedDate, 0);
	}

//	public static Date decodeTime(int encodedTime) {
//		return decodeDateTime(0, encodedTime);
//	}
	
	public static Date decodeDateTime(int encodedDate, int encodedTime) {
		if (encodedDate == 0 && encodedTime == 0)
			return null;
		
		GregorianCalendar calendar = new GregorianCalendar();
		String code;
		
		boolean resetDate = true;
		if (encodedDate != 0) {			
			code = String.valueOf(encodedDate);
			if (code.length() != 7)
				return null;
				
			int year = 2000 + Integer.valueOf(code.substring(1, 3)).intValue();
			calendar.set(Calendar.YEAR, year);
			
			int month = Integer.valueOf(code.substring(3, 5)).intValue();
			calendar.set(Calendar.MONTH, month-1);
			
			int day = Integer.valueOf(code.substring(5, 7)).intValue();
			calendar.set(Calendar.DATE, day);
			
			resetDate = false;
		}		
		if (resetDate) {
			calendar.set(Calendar.YEAR, 0);
			calendar.set(Calendar.MONTH, 0);
			calendar.set(Calendar.DATE, 0);
		}
		
		boolean resetTime = true;
		if (encodedTime != 0) {		
			code = String.valueOf(encodedTime);
			if (code.length() == 5)
				code = "0" + code;
			
			if (code.length() == 6) {
				int hours = Integer.valueOf(code.substring(0, 2)).intValue();
				calendar.set(Calendar.HOUR, hours);
				
				int minutes = Integer.valueOf(code.substring(2, 4)).intValue();
				calendar.set(Calendar.MINUTE, minutes);
				
				int seconds = Integer.valueOf(code.substring(4, 6)).intValue();
				calendar.set(Calendar.SECOND, seconds);
				
				resetTime = false;
			}
		}
		if (resetTime) {		
			calendar.set(Calendar.HOUR, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
		}
		
		Date date = calendar.getTime();
		
		return date; 
	}
	
	public static BigDecimal decodeAmount(BigDecimal amount, int decimalPlaces) {		
		amount = amount.movePointLeft(decimalPlaces);
		return amount.setScale(decimalPlaces, BigDecimal.ROUND_HALF_UP);			
	}

	public static BigDecimal encodeAmount(BigDecimal amount, int decimalPlaces) {		
		amount = amount.movePointRight(decimalPlaces);
		return amount.setScale(decimalPlaces, BigDecimal.ROUND_HALF_UP);			
	}
	
	private final static double round_factor[] = {0.0, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0, 10000000.0, 100000000.0, 1000000000.0};
	
	public static double round(double value, int decimalPlaces)
	{
		if (decimalPlaces > 0 && decimalPlaces < round_factor.length)
			return (Math.round(value*round_factor[decimalPlaces])/round_factor[decimalPlaces]);
		else if (decimalPlaces == 0)
			return Math.round(value);
		else
			return value;
	}
	
	public static int getDecimalPlaces() {
		return 2;
	}

	public static int getDecimalPlaces(String currencyCode) {
		return 2;
	}
	
	public final static String handleNull(String value) {
		return ((value != null) ? value : "");	
	}

	public final static BigDecimal handleNull(BigDecimal value) {
		return ((value != null) ? value : new BigDecimal(0.0));	
	}
}
