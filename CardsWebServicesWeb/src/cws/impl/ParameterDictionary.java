package cws.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import cws.log.Logger;

public class ParameterDictionary {
	HashMap cardAccountTypesGR;
	HashMap cardAccountTypesEN;
	HashMap postingClassesGR;
	HashMap postingClassesEN;
	HashMap transactionTypesGR;
	HashMap transactionTypesEN;

	public ParameterDictionary() {
		
		try {		
			loadCardAccountTypes();
		}
		catch (Exception ex) {
			Logger.logException(ex);
		}
		
		try {
			loadPostingClasses();
		}
		catch (Exception ex) {
			Logger.logException(ex);
		}
		
		try {
			loadTransactionTypes();
		}
		catch (Exception ex) {
			Logger.logException(ex);
		}
	}
	
	public String getCardAccountTypeGR(String code) {
		String description = (String) cardAccountTypesGR.get(code);
		return MessageImpl.handleNull(description); 
	}

	public String getCardAccountTypeEN(String code) {
		String description = (String) cardAccountTypesEN.get(code);
		return MessageImpl.handleNull(description);
	}

	public String getPostingClassGR(String code) {
		String description = (String) postingClassesGR.get(code);
		return MessageImpl.handleNull(description);
	}

	public String getPostingClassEN(String code) {
		String description = (String) postingClassesEN.get(code);
		return MessageImpl.handleNull(description);
	}

	public String getTransactionTypeGR(String code) {
		String description = (String) transactionTypesGR.get(code);
		return MessageImpl.handleNull(description);
	}

	public String getTransactionTypeEN(String code) {
		String description = (String) transactionTypesEN.get(code);
		return MessageImpl.handleNull(description);
	}
	
	protected void loadCardAccountTypes() 
	throws SQLException {
		
		cardAccountTypesGR = new HashMap();
		cardAccountTypesEN = new HashMap();
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs  = null;
		int index = 0;
		try {
			con = CWServer.getInstance().getAS400().getDBConnection();
	
			String sql = "SELECT "					 
				+ "CBCTYP, CBDESC, CBSDSC "
				+ "FROM " + getCardsLibrary() + ".CBINCT0P "
				+ "WHERE CBPART='002'";                 	
			ps = con.prepareStatement(sql);							
			rs = ps.executeQuery();						
							
			ResultSetIterator iter = new ResultSetIterator(rs, 1, 1000);			
			if (iter.hasRows()) {
				String code, descr;
				do {
					code = rs.getString("CBCTYP");
					descr = rs.getString("CBDESC");
					cardAccountTypesGR.put(code, descr);               
					cardAccountTypesEN.put(code, descr);														
				} while (iter.next());
			}	 			
		}
		finally {
			closeObjects(con, ps, rs);
		}				
	}
	
	protected void loadPostingClasses() 
	throws SQLException {
			
		postingClassesGR = new HashMap();
		postingClassesEN = new HashMap();

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs  = null;
		int index = 0;
		try {
			con = CWServer.getInstance().getAS400().getDBConnection();
	
			String sql = "SELECT "					 
				+ "PSCODE, PSSDSC "
				+ "FROM " + getCardsLibrary() + ".CTRNPS0P ";				                 	
			ps = con.prepareStatement(sql);							
			rs = ps.executeQuery();						
							
			ResultSetIterator iter = new ResultSetIterator(rs, 1, 1000);			
			if (iter.hasRows()) {
				String code, descr;
				do {
					code = rs.getString("PSCODE");
					descr = rs.getString("PSSDSC");
					postingClassesGR.put(code, descr);               
					postingClassesEN.put(code, descr);														
				} while (iter.next());
			}	 			
		}
		finally {
			closeObjects(con, ps, rs);
		}				
	}
	
	protected void loadTransactionTypes() 
	throws SQLException {
			
		transactionTypesGR = new HashMap();		
		transactionTypesEN = new HashMap();

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs  = null;
		int index = 0;
		try {
			con = CWServer.getInstance().getAS400().getDBConnection();
	
			String sql = "SELECT "					 
				+ "TTTRTY, TTDESC "
				+ "FROM " + getCardsLibrary() + ".CTRNTY0P ";				                 
			ps = con.prepareStatement(sql);							
			rs = ps.executeQuery();						
							
			ResultSetIterator iter = new ResultSetIterator(rs, 1, 1000);			
			if (iter.hasRows()) {
				String code, descr;
				do {
					code = rs.getString("TTTRTY");
					descr = rs.getString("TTDESC");
					transactionTypesGR.put(code, descr);               
					transactionTypesEN.put(code, descr);														
				} while (iter.next());
			}	 			
		}
		finally {
			closeObjects(con, ps, rs);
		}				
	}
	
	protected static String getCardsLibrary() {	
		return CWServer.getInstance().getSetting("CWS_CARDS_LIBRARY");
	}
	
	protected static void closeObjects(Connection con, PreparedStatement ps, ResultSet rs) {
		
		try {							
			if (rs != null)
				rs.close();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}

		try {							
			if (ps != null)
				ps.close();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}

		try {							
			if (con != null)
				con.close();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}	
}
