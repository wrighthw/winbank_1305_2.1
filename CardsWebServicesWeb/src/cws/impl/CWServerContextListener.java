package cws.impl;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class CWServerContextListener implements ServletContextListener {

	public void contextDestroyed(ServletContextEvent arg0) {		
		CWServer.shutDown();
	}

	public void contextInitialized(ServletContextEvent arg0) {
		CWServer.startUp();
	}
}
