package cws.impl.cred;

public class CredentialsData {
	boolean validCardNo;
	boolean validCardExpiryDate;
	boolean validCVV2;
	boolean validOwnerBirthDate;
	boolean validEmbossName;

	public CredentialsData() {
	}
	
	public boolean isValidCardExpiryDate() {
		return validCardExpiryDate;
	}

	public boolean isValidCardNo() {
		return validCardNo;
	}

	public boolean isValidCVV2() {
		return validCVV2;
	}

	public boolean isValidEmbossName() {
		return validEmbossName;
	}

	public boolean isValidOwnerBirthDate() {
		return validOwnerBirthDate;
	}
}
