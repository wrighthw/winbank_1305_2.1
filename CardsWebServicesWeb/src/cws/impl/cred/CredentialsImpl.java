package cws.impl.cred;

import java.util.Date;
import java.util.HashMap;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.QSYSObjectPathName;

import cws.impl.CWServer;
import cws.impl.MessageImpl;
import cws.impl.as400.AS400Server;
import cws.impl.as400.CharacterParameter;
import cws.impl.as400.Parameter;

/*

Credentials

Input

MSGNUM  CHAR        2    01
CHPART  CHAR        3    Participant ID             
        CHAR       19    Card Number                
        CHAR        7    Card Expiry Date
        CHAR        3    CVV2
        CHAR        7    Card Owner Birth Date
        CHAR       40    Card Owner Emboss Name

Output
CHCARD  CHAR        1    Validation: Card Number ('E'=error, ' '=valid)                
CHCDUD  CHAR        1    Validation: Card Due Date ('E'=error, ' '=valid)
CHCVV2  CHAR        1    Validation: CVV2 ('E'=error, ' '=valid)
CHCOBD  CHAR        1    Validation: Card Owner Birth Date ('E'=error, ' '=valid)
                 
Comments
In case of an AddOn (CHPRIM=�A�) the structure will be the same but only CHPRIM  and AMACCT will be populated. 
In AMACCT will stored the account in which the Card belongs.

Call RPG 

*/

public class CredentialsImpl extends MessageImpl {

	public static CredentialsData execute(
		String participantID,	// CHAR        3    Participant ID
		String cardNo,			// CHAR       19    Card Number
		String cardExpiryDate, 	// CHAR        4    Card Expiry Date
		String CVV2, 			// CHAR        3    CVV2 
		Date ownerBirthDate, 	// CHAR        7    Card Owner Birth Date
		String ownerEmbossName	// CHAR       40    Card Owner Emboss Name
	) 
	throws Exception {

		/*
		participantID = "002";                   
		cardNo = "4027980000009104";
		cardExpiryDate = ??
		CVV2 = "";
		ownerBirthDate = ??
		ownerEmbossName = ??        		
		*/		

		CWServer cwsServer = CWServer.getInstance();		  				
		AS400Server server = cwsServer.getAS400();
		
		CredentialsData data = new CredentialsData();
		AS400 con = null;
		try {
			con = server.openPGMConnection();
						
			server.runCommand(con, cwsServer.getSetting("CWS_CARDS_INIT"));
			
			String pgmLib = cwsServer.getSetting("CWS_CARDS_CRED_PGM_LIB");
			String pgm = cwsServer.getSetting("CWS_CARDS_CRED_PGM");
			String program = QSYSObjectPathName.toPath(pgmLib, pgm, "PGM");
			if (cardExpiryDate.length() == 4) 
				cardExpiryDate = cardExpiryDate.substring(2, 4) + cardExpiryDate.substring(0, 2); 			
			Parameter[] input = new Parameter[] {
				new CharacterParameter("MSGNUM", "01", 2),				// (Message number)
				new CharacterParameter("__PART", participantID, 3),		// Participant ID      
				new CharacterParameter("__CARD", cardNo, 19),			// Card Number   				
				new CharacterParameter("__CDUD", cardExpiryDate, 4),	// Card Expiry Date
				new CharacterParameter("__CVV2", CVV2, 3),				// CVV2
				new CharacterParameter("__COBD", ownerBirthDate),		// Card Owner Birth Date
				new CharacterParameter("__COEN", ownerEmbossName, 40)	// Card Owner Emboss Name
			};
			Parameter[] output = new Parameter[] {								
				new CharacterParameter("_VCARD", 1),					// Validation: Card Number   				
				new CharacterParameter("_VCDUD", 1),					// Validation: Card Due Date
				new CharacterParameter("_VCVV2", 1),					// Validation: CVV2
				new CharacterParameter("_VCOBD", 1),					// Validation: Card Owner Birth Date
				new CharacterParameter("_VCOEN", 1)						// Validation: Card Owner Emboss Name
			};
												
			HashMap result = server.runProgram(con, program, input, output); 
			data.validCardNo = !(handleNull((String) result.get("_VCARD"))).equals("E");
			data.validCardExpiryDate = !(handleNull((String) result.get("_VCDUD"))).equals("E"); 
			data.validCVV2 = !(handleNull((String) result.get("_VCVV2"))).equals("E"); 
			data.validOwnerBirthDate = !(handleNull((String) result.get("_VCOBD"))).equals("E");
			data.validEmbossName = !(handleNull((String) result.get("_VCOEN"))).equals("E");
			
			if (!data.validCardNo) {
				data.validCardExpiryDate = data.validCVV2 = data.validOwnerBirthDate = data.validEmbossName = false; 
			}
		}
		finally {
			if (con != null)
				server.closePGMConnection(con);
		}

		return data;
	}
}
