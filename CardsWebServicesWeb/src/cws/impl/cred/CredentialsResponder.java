package cws.impl.cred;

import java.util.Date;

import cws.MessageResponder;
import cws.XmlTools;
import cws.impl.Statistics;
import cws.log.Logger;

public class CredentialsResponder extends MessageResponder {

	public String processRequest(String message, String dataXML, String extraDataXML) throws Exception {	

//		Date date1 = new Date();

		int[] positions = new int[2];
		
		String participantID = XmlTools.parseTag(dataXML, "participantID", positions);		
		String cardNo = XmlTools.parseTag(dataXML, "cardNo", positions); 
		String cardExpiryDate = XmlTools.parseTag(dataXML, "cardExpiryDate", positions);
		String CVV2 = XmlTools.parseTag(dataXML, "CVV2", positions);
		Date ownerBirthDate = stringToDate(XmlTools.parseTag(dataXML, "ownerBirthDate", positions));
		String ownerEmbossName = XmlTools.parseTag(dataXML, "ownerEmbossName", positions);
				 											
		CredentialsData data = verifyCredentials(participantID, cardNo, cardExpiryDate, 
													CVV2, ownerBirthDate, ownerEmbossName);
															
		String responseXML = generateResponseXML(data);		
		
//		Date date2 = new Date();	
//		
//		Logger.logTimeAndRequestMessage("verifyCredentials","cardNo:"+cardNo, date1, date2);
				
		return responseXML;		
	}

	protected CredentialsData verifyCredentials(String participantID, String cardNo, String cardExpiryDate, 
													String CVV2, Date ownerBirthDate, String ownerEmbossName) 
	throws Exception {
		
		CredentialsData data = null;
		try {
			long startTime = System.currentTimeMillis();		
			data = CredentialsImpl.execute(participantID, cardNo, cardExpiryDate, CVV2, ownerBirthDate, ownerEmbossName);			 
			long endTime = System.currentTimeMillis();
			Statistics.addRequest("verifyCredentials", endTime - startTime);
		}
		catch (Exception ex) {
			Logger.logException(ex);
			data = new CredentialsData();			 
		}
											
		return data; 
	}

	protected String generateResponseXML(CredentialsData data) {	
		StringBuffer dataXML = new StringBuffer();
		appendStructure(dataXML, data);
	
		return generateResponseXMLStructure("verifyCredentialsResponse", "verifyCredentialsReturn",  
											"http://cred.impl.cws",
											"CredentialsData", dataXML.toString());			
	}

	protected void appendStructure(StringBuffer dataXML, CredentialsData data) {		
														
		appendBoolean(dataXML, "validCardExpiryDate", data.isValidCardExpiryDate());
		appendBoolean(dataXML, "validCardNo", data.isValidCardNo());
		appendBoolean(dataXML, "validCVV2", data.isValidCVV2());
		appendBoolean(dataXML, "validEmbossName", data.isValidEmbossName());
		appendBoolean(dataXML, "validOwnerBirthDate", data.isValidOwnerBirthDate());
	}
}
