package cws.impl.hist;

import java.math.BigDecimal;
import java.util.Date;

import cws.impl.CWServer;
import cws.impl.MessageImpl;
import cws.impl.ParameterDictionary;

public class TransHistoryData {
	int recordCount;
	String ParticipantID;		// HIPART  CHAR        3     Participant Id               
	String accountNo;			// HIACCT  CHAR       19     Account #   
	String billCurrencyCode;	// HIABLC  CHAR        3     Account Billing Curr Code                   
	int billDate;				// HIBLDT  DECIMAL     7     Billing Date
		Date billDateMsg;	                 
	int processDate;			// HIPRDT  DECIMAL     7     Process Date
		Date processDateMsg;                 
	int transDate;				// HITRDT  DECIMAL     7     Transaction Date
		Date transDateMsg;             
	String cardNo;				// HICARD  CHAR       19     Card #                       
	String referenceNo;			// HIREFN  CHAR       12     Reference #                  
	String approvalNo;			// HIAPRN  CHAR        6     Approval #                   
	int reversalInd;			// HIRVSL  NUMERIC     1     Reversal Indicator           
	String stmtExclusion;		// HISTMF  CHAR        1     Statement Exclusion Flag         
	String transType;			// HITRTY  CHAR        2     Transaction Type
	String transTypeDescrGR;                 
	String transTypeDescrEN;
	String debitCredit;			// HIDBCR  CHAR        1     Debit/Credit Flag                
	String transCurrencyCode;	// HITRCC  CHAR        2     Transaction Curr Code            
	BigDecimal transAmount;		// HITRNA  DECIMAL    15     Transaction Amount
		BigDecimal transAmountMsg;
	BigDecimal billAmount;		// HIABLA  DECIMAL    15     Account Billing Amount
		BigDecimal billAmountMsg;           
	String transOrgCode;		// HIORGN  CHAR        3     Transaction Origin Code          
	String cardAcceptDescr;		// HICADS  CHAR       30     Card Acceptor Description        
	String postingDescr1;		// HIPDS1  CHAR       40     Posting Description 1            
	String postingDescr2;		// HIPDS2  CHAR       40     Posting Description 2
	
	public TransHistoryData() {            
	}
	
	public String getAccountNo() {
		return accountNo;
	}

	public String getApprovalNo() {
		return approvalNo;
	}

	public BigDecimal getBillAmount() {
		return billAmountMsg;
	}

	public String getBillCurrencyCode() {
		return billCurrencyCode;
	}

	public Date getBillDate() {
		return billDateMsg;
	}

	public String getCardAcceptDescr() {
		return cardAcceptDescr;
	}

	public String getCardNo() {
		return cardNo;
	}

	public String getDebitCredit() {
		return debitCredit;
	}

	public String getParticipantID() {
		return ParticipantID;
	}

	public String getPostingDescr1() {
		return postingDescr1;
	}

	public String getPostingDescr2() {
		return postingDescr2;
	}

	public Date getProcessDate() {
		return processDateMsg;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public int getReversalInd() {
		return reversalInd;
	}

	public String getStmtExclusion() {
		return stmtExclusion;
	}

	public BigDecimal getTransAmount() {
		return transAmountMsg;
	}

	public String getTransCurrencyCode() {
		return transCurrencyCode;
	}

	public Date getTransDate() {
		return transDateMsg;
	}

	public String getTransOrgCode() {
		return transOrgCode;
	}

	public String getTransType() {
		return transType;
	}
	
	public int getRecordCount() {
		return recordCount;
	}

	public String getTransTypeDescrEN() {
		return transTypeDescrEN;
	}

	public String getTransTypeDescrGR() {
		return transTypeDescrGR;
	}
	
	public void transform() {		
		billDateMsg = MessageImpl.decodeDate(billDate);
		processDateMsg = MessageImpl.decodeDate(processDate);
		transDateMsg = MessageImpl.decodeDate(transDate);
		transAmountMsg = MessageImpl.decodeAmount(transAmount, MessageImpl.getDecimalPlaces());
		billAmountMsg = MessageImpl.decodeAmount(billAmount, MessageImpl.getDecimalPlaces());
		ParameterDictionary dict = CWServer.getInstance().getDictionary();
		transTypeDescrGR = dict.getTransactionTypeGR(transType); 
		transTypeDescrEN = dict.getTransactionTypeEN(transType); 		
	}	
}
