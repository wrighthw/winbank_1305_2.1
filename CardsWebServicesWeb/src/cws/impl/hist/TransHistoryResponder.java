package cws.impl.hist;

import java.util.Date;

import cws.MessageResponder;
import cws.XmlTools;
import cws.impl.Statistics;
import cws.log.Logger;

public class TransHistoryResponder extends MessageResponder {

	public String processRequest(String message, String dataXML, String extraDataXML) throws Exception {	

//		Date date1 = new Date();

		int[] positions = new int[2];
		
		String participantID = XmlTools.parseTag(dataXML, "participantID", positions);					
		String accountNo = XmlTools.parseTag(dataXML, "accountNo", positions); 
		String cardNo = XmlTools.parseTag(dataXML, "cardNo", positions);
		String billCurrencyCode = XmlTools.parseTag(dataXML, "billCurrencyCode", positions);
		Date billDateFrom = stringToDate(XmlTools.parseTag(dataXML, "billDateFrom", positions)); 
		Date billDateTo = stringToDate(XmlTools.parseTag(dataXML, "billDateTo", positions));
		boolean sortByCardNo = Boolean.valueOf(XmlTools.parseTag(dataXML, "sortByCardNo", positions)).booleanValue();
		int start = Integer.parseInt(XmlTools.parseTag(dataXML, "start", positions));
		int count = Integer.parseInt(XmlTools.parseTag(dataXML, "count", positions));																									
						 													
		TransHistoryData[] rows = getTransHistory(participantID, accountNo, cardNo, billCurrencyCode,
													billDateFrom, billDateTo, sortByCardNo, start, count); 
																																
		String responseXML = generateResponseXML(rows);		
		
//		Date date2 = new Date();	
//		
//		Logger.logTimeAndRequestMessage("getTransHistory","accountNo:"+accountNo, date1, date2);
		
		return responseXML;		
	}

	protected TransHistoryData[] getTransHistory(String participantID, String accountNo, String cardNo, 
													String billCurrencyCode,
													Date billDateFrom, Date billDateTo,
													boolean sortByCardNo,
													int start, int count) 
		throws Exception {
		
			TransHistoryData[] history = null;
			try {
				long startTime = System.currentTimeMillis();
				if (cardNo != null && (cardNo.equals("") || cardNo.equals(" ")))
					cardNo = null;			
				history = TransHistoryImpl.execute(participantID, accountNo, cardNo, billCurrencyCode, 
														billDateFrom, billDateTo, sortByCardNo,
														start, count);
				long endTime = System.currentTimeMillis();
				Statistics.addRequest("getTransHistory", endTime - startTime);		
			}
			catch (Exception ex) {
				Logger.logException(ex);
				history = new TransHistoryData[0];			
			}
											
			return history; 
		}
	
	protected String generateResponseXML(TransHistoryData[] rows) {	
		StringBuffer multiRefsXML = new StringBuffer();

		int count = rows.length;
		for (int i = 0; i < count; i++)
			appendArrayItem(multiRefsXML, i, rows[i]);
		
		return generateResponseXMLArray("getTransHistoryResponse", "getTransHistoryReturn",  
											"http://hist.impl.cws",
											"TransHistoryData", count, multiRefsXML.toString());			
	}

	protected void appendArrayItem(StringBuffer dataXML, int id, TransHistoryData data) {
		
		appendMultiRefStart(dataXML, id, "TransHistoryData");
								
		appendString(dataXML, "accountNo", data.getAccountNo());
		appendString(dataXML, "approvalNo", data.getApprovalNo());
		appendDecimal(dataXML, "billAmount", data.getBillAmount());
		appendString(dataXML, "billCurrencyCode", data.getBillCurrencyCode());
		appendDate(dataXML, "billDate", data.getBillDate());
		appendString(dataXML, "cardAcceptDescr", encodeString(data.getCardAcceptDescr()));
		appendString(dataXML, "cardNo", data.getCardNo());
		appendString(dataXML, "debitCredit", data.getDebitCredit());
		appendString(dataXML, "participantID", data.getParticipantID());
		appendString(dataXML, "postingDescr1", encodeString(data.getPostingDescr1()));
		appendString(dataXML, "postingDescr2", encodeString(data.getPostingDescr2()));
		appendDate(dataXML, "processDate", data.getProcessDate());
		appendString(dataXML, "referenceNo", data.getReferenceNo());
		appendInteger(dataXML, "reversalInd", data.getReversalInd());
		appendString(dataXML, "stmtExclusion", data.getStmtExclusion());
		appendDecimal(dataXML, "transAmount", data.getTransAmount());
		appendString(dataXML, "transCurrencyCode", data.getTransCurrencyCode());
		appendDate(dataXML, "transDate", data.getTransDate());
		appendString(dataXML, "transOrgCode", data.getTransOrgCode());
		appendString(dataXML, "transType", data.getTransType());
		appendInteger(dataXML, "recordCount", data.getRecordCount());
		appendString(dataXML, "transTypeDescrEN", encodeString(data.getTransTypeDescrEN()));
		appendString(dataXML, "transTypeDescrGR", encodeString(data.getTransTypeDescrGR()));
			 
		appendMultiRefEnd(dataXML, "TransHistoryData");		
	}
}
