package cws.impl.hist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import cws.impl.CWServer;
import cws.impl.MessageImpl;
import cws.impl.ResultSetIterator;

/*

Historical Transaction in Statement

Input
MSGNUM  NUMERIC     2     5 
HIPART  CHAR        3     Participant Id               
HIACCT  CHAR       19     Account #                    
HIABLC  CHAR        3     Account Billing Curr Code                   
HIBLDT  DECIMAL     7     Billing Date                 

Output

HIPART  CHAR        3     Participant Id               
HIACCT  CHAR       19     Account #   
HIABLC  CHAR        3     Account Billing Curr Code                   
HIBLDT  DECIMAL     7     Billing Date                 
HIPRDT  DECIMAL     7     Process Date                 
HITRDT  DECIMAL     7     Transaction Date             
HICARD  CHAR       19     Card #                       
HIREFN  CHAR       12     Reference #                  
HIAPRN  CHAR        6     Approval #                   
HIRVSL  NUMERIC     1     Reversal Indicator           
HISTMF  CHAR        1     Statement Exclusion Flag         
HITRTY  CHAR        2     Transaction Type                 
HIDBCR  CHAR        1     Debit/Credit Flag                
HITRCC  CHAR        2     Transaction Curr Code            
HITRNA  DECIMAL    15     Transaction Amount               
HIABLA  DECIMAL    15     Account Billing Amount           
HIORGN  CHAR        3     Transaction Origin Code          
HICADS  CHAR       30     Card Acceptor Description        
HIPDS1  CHAR       40     Posting Description 1            
HIPDS2  CHAR       40     Posting Description 2            

File : CTRNHI0P
	
*/

public class TransHistoryStmtImpl extends MessageImpl {

	public static TransHistoryData[] execute(
		String participantID, 		// HIPART  CHAR        3    Participant ID                   
		String accountNo,     		// HIACCT  CHAR       19    Account #                        
		String cardNo,	     		// HICARD  CHAR       19    Card #
		String billCurrencyCode, 	// HIABLC  CHAR        3    Account Billing Curr Code
		Date billingDate,			// HIBLDT  DECIMAL     7    Billing Date
		boolean sortByCardNo,  			  
		int start,            		// 1-based
		int count		
	) 
	throws SQLException {

		/*
		participantID = "002";                   
		accountNo = "4020682000093008";
		cardNo = "";                        
		billCurrencyCode = "978";
		billingDate = 1031201;
		sortByCardNo = false;			  
		start = 1;
		count = 5;
		*/		

		TransHistoryData[] data = new TransHistoryData[count]; 
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs  = null;
		int index = 0;
		try {
			con = CWServer.getInstance().getAS400().getDBConnection();
			
			/* count rows */
			
			String sql = "SELECT COUNT(*)"
				+ " FROM " + getCardsLibrary() + ".CTRNHI0P"							
				+ " WHERE HIPART = ? AND HIACCT = ? AND HIABLC = ? AND HIBLDT = ?";
			if (cardNo != null)
				sql += " AND HICARD = ?"; 												
			
			ps = con.prepareStatement(sql);	
			int pos = 1;
			ps.setString(pos, participantID); pos++;
			ps.setString(pos, accountNo); pos++;
			ps.setString(pos, billCurrencyCode); pos++;
			ps.setInt(pos, encodeDate(billingDate)); pos++;
			if (cardNo != null) {			
				ps.setString(pos, cardNo); pos++;
			}
			
			rs = ps.executeQuery();
			rs.next();
			int recordCount = rs.getInt(1);			
			
			/* get actual data */
			
			sql = "SELECT"										
				+ " HIPART, HIACCT, HIABLC, HIBLDT, HIPRDT, HITRDT, HICARD,"                       
				+ " HIREFN, HIAPRN, HIRVSL, HISTMF, HITRTY, HIDBCR, HITRCC,"            
				+ " HITRNA, HIABLA, HIORGN, HICADS, HIPDS1, HIPDS2"   			
				+ " FROM " + getCardsLibrary() + ".CTRNHI0P"							
				+ " WHERE HIPART = ? AND HIACCT = ? AND HIABLC = ? AND HIBLDT = ?";
			if (cardNo != null)
				sql += " AND HICARD = ?"; 																
			sql += " ORDER BY";
			if (sortByCardNo)
				sql += " HICARD ASC,";				
			sql += " HIPRDT ASC, HITRDT ASC";
						
			ps = con.prepareStatement(sql);
			pos = 1;		
			ps.setString(pos, participantID); pos++;
			ps.setString(pos, accountNo); pos++;
			ps.setString(pos, billCurrencyCode); pos++;
			ps.setInt(pos, encodeDate(billingDate)); pos++;
			if (cardNo != null) {			
				ps.setString(pos, cardNo); pos++;
			}
			
			rs = ps.executeQuery();			
			
			TransHistoryData entry;			
			ResultSetIterator iter = new ResultSetIterator(rs, start, count);
			if (iter.hasRows()) {
				do {
					entry = new TransHistoryData();
										
					entry.recordCount 		= recordCount;
					entry.ParticipantID 	= handleNull(rs.getString("HIPART"));               
					entry.accountNo 		= handleNull(rs.getString("HIACCT"));   
					entry.billCurrencyCode	= handleNull(rs.getString("HIABLC"));                   
					entry.billDate 			= rs.getInt("HIBLDT");                 
					entry.processDate 		= rs.getInt("HIPRDT");                 
					entry.transDate 		= rs.getInt("HITRDT");             
					entry.cardNo 			= handleNull(rs.getString("HICARD"));                       
					entry.referenceNo 		= handleNull(rs.getString("HIREFN"));                  
					entry.approvalNo 		= handleNull(rs.getString("HIAPRN"));                   
					entry.reversalInd 		= rs.getInt("HIRVSL");           
					entry.stmtExclusion 	= handleNull(rs.getString("HISTMF"));         
					entry.transType 		= handleNull(rs.getString("HITRTY"));
					entry.debitCredit 		= handleNull(rs.getString("HIDBCR"));                
					entry.transCurrencyCode = handleNull(rs.getString("HITRCC"));            
					entry.transAmount 		= handleNull(rs.getBigDecimal("HITRNA"));
					entry.billAmount 		= handleNull(rs.getBigDecimal("HIABLA"));           
					entry.transOrgCode 		= handleNull(rs.getString("HIORGN"));          
					entry.cardAcceptDescr 	= handleNull(rs.getString("HICADS"));        
					entry.postingDescr1 	= handleNull(rs.getString("HIPDS1"));
					entry.postingDescr2 	= handleNull(rs.getString("HIPDS2"));
												              									
					entry.transform();
																	              									
					data[index] = entry;
					index++;
				} while (iter.next());
			}	 			
		}
		finally {
			closeObjects(con, ps, rs);
		}
		
		TransHistoryData[] trimmedData = new TransHistoryData[index];		
		System.arraycopy(data, 0, trimmedData, 0, index);				  
		return trimmedData;
	}
}
