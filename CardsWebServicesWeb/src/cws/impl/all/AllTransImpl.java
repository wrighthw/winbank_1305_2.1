package cws.impl.all;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;

import cws.impl.MessageImpl;
import cws.impl.auth.AuthorizationData;
import cws.impl.auth.AuthorizationImpl;
import cws.impl.hist.TransHistoryData;
import cws.impl.hist.TransHistoryImpl;
import cws.impl.trans.CurrentTransData;
import cws.impl.trans.CurrentTransImpl;

public class AllTransImpl extends MessageImpl {

	public static AllTransData[] execute(
		String participantID, 		// xxPART  CHAR        3    Participant ID                   
		String accountNo,     		// xxACCT  CHAR       19    Account #                       
		String cardNo,     			// xxCARD  CHAR       19    Card #
		String billCurrencyCode, 	// xxABLC  CHAR        3    Account Billing Curr Code
		Date dateFrom,			  			  
		Date dateTo,
		final boolean sortByCardNo,
		int start,            		// 1-based
		int count		
	) 
	throws SQLException {

		/*
		participantID = "002";                   
		accountNo = "4908450208887002";    
		cardNo = "";                    
		billCurrencyCode = "978";
		dateFrom = new Date(103, 1, 1);			  
		dateTo = new Date(104, 1, 1);
		sortByCardNo = false;
		start = 1;
		count = 5;
		*/		
	
		int limit = Math.min(start+count, 5000);
		int totalCount = 0;
		
		CurrentTransData[] currentData = CurrentTransImpl.execute(
			participantID, accountNo, cardNo, billCurrencyCode, dateFrom ,dateTo, sortByCardNo, 1, limit);
		if (currentData.length > 0)
			totalCount += currentData[0].getRecordCount(); 
			
		AuthorizationData[] authData = AuthorizationImpl.execute(
			participantID, accountNo, cardNo, billCurrencyCode, dateFrom , dateTo, sortByCardNo, 1, limit); 
		if (authData.length > 0)
			totalCount += authData[0].getRecordCount(); 
		
		TransHistoryData[] histData = TransHistoryImpl.execute(
			participantID, accountNo, cardNo, billCurrencyCode, dateFrom, dateTo, sortByCardNo, 1, limit);
		if (histData.length > 0)
			totalCount += histData[0].getRecordCount(); 		
		
		int returnedCount = currentData.length + authData.length + histData.length;
		AllTransData[] allData = new AllTransData[returnedCount]; 
		
		int index = 0;
		index = convertData(currentData, allData, index, totalCount);
		index = convertData(authData, allData, index, totalCount);
		index = convertData(histData, allData, index, totalCount);
				
		Arrays.sort(allData, new Comparator() {
			public int compare(Object o1, Object o2) {
				
				// CARD-NO ASC
				
				if (sortByCardNo) {								
					String c1 = ((AllTransData) o1).getCardNo();				
					String c2 = ((AllTransData) o2).getCardNo();
					
					int r = c1.compareTo(c2);
					if (r != 0)
						return r;
				}								
				
				// PROCESS-DATE DESC
				
				Date d1 = ((AllTransData) o1).getProcessDate();				
				long time1 = (d1 != null) ? d1.getTime() : 0;				
				
				Date d2 = ((AllTransData) o2).getProcessDate();				
				long time2 = (d2 != null) ? d2.getTime() : 0;
												
				if (time1 > time2)				
					return -1;
				else if (time1 < time2)
					return 1;
				
				// TRANSACTION-DATE DESC
				
				d1 = ((AllTransData) o1).getTransDate();
				time1 = (d1 != null) ? d1.getTime() : 0;
			
				d2 = ((AllTransData) o2).getTransDate();
				time2 = (d2 != null) ? d2.getTime() : 0;
					
				if (time1 > time2)				
					return -1;
				else if (time1 < time2)
					return 1;
				else
					return 0;				
			}

			public boolean equals(Object obj) {
				return false;
			}
		});
		
		count = Math.min(returnedCount-(start-1), count);
		if (count < 0)
			count = 0;
		AllTransData[] data = new AllTransData[count];				
		if (count > 0)
			System.arraycopy(allData, start-1, data, 0, count);
		   
		return data;
	}	
		
	protected static int convertData(CurrentTransData[] inData, AllTransData[] outData, int start, int totalCount) {		
		int count = inData.length;
		
		int index;		
		for (int i = 0; i < count; i++) {
			index = start+i;
			
			outData[index] = new AllTransData();
			
			outData[index].recordCount = totalCount;
			outData[index].recordType = "C";
			outData[index].participantID = inData[i].getParticipantID();               
			outData[index].accountNo = inData[i].getAccountNo();   
			outData[index].billCurrencyCode = inData[i].getBillCurrencyCode();                
			outData[index].billDate = 0;				                 
			outData[index].processDate = encodeDate(inData[i].getProcessDate());			                 
			outData[index].transDate = encodeDate(inData[i].getTransDate());
			outData[index].transTime = 0;			             
			outData[index].cardNo = inData[i].getCardNo();                       
			outData[index].referenceNo = inData[i].getReferenceNo();                  
			outData[index].approvalNo = inData[i].getApprovalNo();                   
			outData[index].reversalInd = inData[i].getReversalInd();           
			outData[index].stmtExclusion = inData[i].getRemoveFromStmt();         
			outData[index].transType = inData[i].getTransType();
			outData[index].transTypeDescrGR = inData[i].getTransTypeDescrGR();                 
			outData[index].transTypeDescrEN = inData[i].getTransTypeDescrEN();
			outData[index].postingClass = MessageImpl.handleNull((String) null);
			outData[index].postingClassDescrGR = MessageImpl.handleNull((String) null);
			outData[index].postingClassDescrEN = MessageImpl.handleNull((String) null);
			outData[index].debitCredit = inData[i].getDebitCredit();                
			outData[index].transCurrencyCode = inData[i].getTransCurrencyCode();            
			outData[index].transAmount = encodeAmount(inData[i].getTransAmount(), getDecimalPlaces());			
			outData[index].billAmount = encodeAmount(inData[i].getAccBillAmount(), getDecimalPlaces());			           
			outData[index].transOrgCode = inData[i].getTransOrgCode();          
			outData[index].cardAcceptDescr = inData[i].getCardAcceptDescr();        
			outData[index].postingDescr1 = inData[i].getPostingDescr1();            
			outData[index].postingDescr2 = inData[i].getPostingDescr2();
			
			outData[index].transform();
		}			
		
		return (start + count);
	}

	protected static int convertData(AuthorizationData[] inData, AllTransData[] outData, int start, int totalCount) {		
		int count = inData.length;
		
		int index;		
		for (int i = 0; i < count; i++) {
			index = start+i;
			
			outData[index] = new AllTransData();
			
			outData[index].recordCount = totalCount;
			outData[index].recordType = "A";
			outData[index].participantID = inData[i].getParticipantID();               
			outData[index].accountNo = inData[i].getAccountNo();   
			outData[index].billCurrencyCode = MessageImpl.handleNull((String) null);
			outData[index].billDate = 0;				                 
			outData[index].processDate = encodeDate(inData[i].getTransDateTime());			                 
			outData[index].transDate = encodeDate(inData[i].getTransDateTime());
			outData[index].transTime = encodeTime(inData[i].getTransDateTime());			             
			outData[index].cardNo = inData[i].getCardNo();                       
			outData[index].referenceNo = inData[i].getReferenceNo();                  
			outData[index].approvalNo = inData[i].getApprovalNo();                   
			outData[index].reversalInd = 0;           
			outData[index].stmtExclusion = MessageImpl.handleNull((String) null);        
			outData[index].transType = MessageImpl.handleNull((String) null);
			outData[index].transTypeDescrGR = MessageImpl.handleNull((String) null);
			outData[index].transTypeDescrEN = MessageImpl.handleNull((String) null);
			outData[index].postingClass = inData[i].getPostingClass();
			outData[index].postingClassDescrGR = inData[i].getPostingClassDescrGR();
			outData[index].postingClassDescrEN = inData[i].getPostingClassDescrEN();	
			outData[index].debitCredit = inData[i].getDebitCredit();                
			outData[index].transCurrencyCode = inData[i].getCurrencyCode();            
			outData[index].transAmount = encodeAmount(inData[i].getTransAmount(), getDecimalPlaces());			
			outData[index].billAmount = encodeAmount(inData[i].getTransActualAmount(), getDecimalPlaces());	           
			outData[index].transOrgCode = MessageImpl.handleNull((String) null);
			outData[index].cardAcceptDescr = MessageImpl.handleNull((String) null);
			outData[index].postingDescr1 = inData[i].getAuthDescription();            
			outData[index].postingDescr2 = MessageImpl.handleNull((String) null);
			
			outData[index].transform();
		}			
		
		return (start + count);
	}

	protected static int convertData(TransHistoryData[] inData, AllTransData[] outData, int start, int totalCount) {		
		int count = inData.length;
		
		int index;		
		for (int i = 0; i < count; i++) {
			index = start+i;
			
			outData[index] = new AllTransData();
			
			outData[index].recordCount = totalCount;
			outData[index].recordType = "H";
			outData[index].participantID = inData[i].getParticipantID();               
			outData[index].accountNo = inData[i].getAccountNo();   
			outData[index].billCurrencyCode = inData[i].getBillCurrencyCode();                
			outData[index].billDate = 0;				                 
			outData[index].processDate = encodeDate(inData[i].getProcessDate());			                 
			outData[index].transDate = encodeDate(inData[i].getTransDate());
			outData[index].transTime = 0;			             
			outData[index].cardNo = inData[i].getCardNo();                       
			outData[index].referenceNo = inData[i].getReferenceNo();                  
			outData[index].approvalNo = inData[i].getApprovalNo();                   
			outData[index].reversalInd = inData[i].getReversalInd();           
			outData[index].stmtExclusion = inData[i].getStmtExclusion();         
			outData[index].transType = inData[i].getTransType();
			outData[index].transTypeDescrGR = inData[i].getTransTypeDescrGR();                 
			outData[index].transTypeDescrEN = inData[i].getTransTypeDescrEN();
			outData[index].postingClass = MessageImpl.handleNull((String) null);
			outData[index].postingClassDescrGR = MessageImpl.handleNull((String) null);
			outData[index].postingClassDescrEN = MessageImpl.handleNull((String) null);
			outData[index].debitCredit = inData[i].getDebitCredit();                
			outData[index].transCurrencyCode = inData[i].getTransCurrencyCode();            
			outData[index].transAmount = encodeAmount(inData[i].getTransAmount(), getDecimalPlaces());			
			outData[index].billAmount = encodeAmount(inData[i].getBillAmount(), getDecimalPlaces());			           
			outData[index].transOrgCode = inData[i].getTransOrgCode();          
			outData[index].cardAcceptDescr = inData[i].getCardAcceptDescr();        
			outData[index].postingDescr1 = inData[i].getPostingDescr1();            
			outData[index].postingDescr2 = inData[i].getPostingDescr2();
			
			outData[index].transform();
		}			
		
		return (start + count);
	}
}
