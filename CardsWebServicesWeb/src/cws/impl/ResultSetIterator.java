package cws.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ResultSetIterator {
	ResultSet rs;
	int startIndex;
	int endIndex;
	boolean hasRows;

	/* 
	Usage example:
		ResultSetIterator iter = new ResultSetIterator(rs, 1, 10);
		if (iter.hasRows()) {
			do {
				rs.getXXXX(...);
			
			} while (iter.next());
		}	 
	*/

	public ResultSetIterator(ResultSet rs, int start, int count) 
	throws SQLException {
		
		this.rs = rs;
		this.startIndex = start-1;
		this.endIndex = startIndex + count;	
		
		this.hasRows = false;
		
		if (rs.next()) {					
		
			if (startIndex >= 0) {
				int r = rs.getRow();
				while (r <= startIndex && r > 0) {
					rs.next();
					r = rs.getRow();
				}
				if (r == 0)
					hasRows = false;
			}
			hasRows = true;
		}			
	}

	public boolean hasRows() {
		return hasRows;
	}

	public boolean next()
	throws SQLException {
		
		if (hasRows)
			return (rs.getRow() < endIndex && rs.next());
		else	
			return false;
	}
}
