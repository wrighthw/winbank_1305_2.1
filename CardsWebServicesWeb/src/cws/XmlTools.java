package cws;

public class XmlTools {
	
	public static String extractMessageName(String xml) {
	
		int pos1 = xml.indexOf(' ');
		int pos2 = xml.indexOf('>');
		String message;
		if (pos2 < pos1)
			message = xml.substring(1, pos2);
		else
			message = xml.substring(1, pos1);
			
		int pos = message.indexOf(':');
		if (pos >= 0)
			message = message.substring(pos+1);
			
		return message;
	}
		
	public static String parseTag(String xml, String tag, int[] positions) {
		
		int pos2 = xml.indexOf(tag);
		if (pos2 < 0)
			return null;
		if (xml.charAt(pos2-1) == ':') {		
			int pos1 = xml.lastIndexOf('<', pos2);
			String prefix = xml.substring(pos1+1, pos2);
			tag = prefix + tag;
		} 
	
		positions[0] = positions[1] = 0; 
	
		String emptyTag = "<"+tag+"/>";
		int pos = xml.indexOf(emptyTag);
		if (pos >= 0)
			return "";
	
		String startTag = "<"+tag;		
		pos = xml.indexOf(startTag);		
		if (pos >= 0) {						
			positions[0] = pos;
			positions[1] = pos + startTag.length();
			xml = xml.substring(pos + startTag.length());
			pos = xml.indexOf(">");
			if (pos >= 0) {
				if (pos > 0 && xml.indexOf("/>") == pos-1)
					return "";
									
				positions[1] += pos + 1;
				xml = xml.substring(pos + 1);
				String endTag = "</"+tag+">";					
				pos = xml.indexOf(endTag);
				if (pos >= 0) {		
					positions[1] += pos + endTag.length();	
					xml = xml.substring(0, pos);
					return xml;
				}
			}			 								
		}	
	
		positions[0] = positions[1] = 0; 
		return null;		
	}		
}
