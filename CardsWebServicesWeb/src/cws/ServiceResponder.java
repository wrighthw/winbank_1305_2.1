package cws;


import cws.impl.CWServer;
import cws.impl.Statistics;


public class ServiceResponder extends MessageResponder {
	public static final String serviceName = "CardsWebService";
	public static final String serviceVersion = "1.0.7 build 10p";

	public String processRequest(String message, String dataXML, String extraDataXML) throws Exception {	

		int[] positions = new int[2];
		
		String responseXML = "";
		if (message.equals("ping")) {		
			responseXML = generateResponseXML("pingResponse", "pingReturn", ping());	
		}		
		else if (message.equals("echo")) {
			String echoMessage = XmlTools.parseTag(dataXML, "message", positions);
			responseXML = generateResponseXML("echoResponse", "echoReturn", echo(echoMessage));
		}
		else if (message.equals("getStatus")) {
			String section = XmlTools.parseTag(dataXML, "section", positions);
			responseXML = generateResponseXML("", "", getStatus(section));
		}						 											
																	
		return responseXML;		
	}

	protected String getStatus(String section) {
		int week = 0;
		if (section.startsWith("week"))
			week = Integer.parseInt(section.substring(4));		
		return Statistics.getStatisticsHTML(week);
	}

	protected String ping() {
		String liveState = (CWServer.getInstance().isLive() ? "live" : "test");
		return serviceName + " " + serviceVersion + " [" + liveState + "]";
	}
	
	protected String echo(String message) {
		return message;
	}
}
