package cws;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;


import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;


import cws.impl.all.AllTransResponder;
import cws.impl.auth.AuthorizationResponder;
import cws.impl.bal.BalancesResponder;
import cws.impl.bill.BillingResponder;
import cws.impl.cred.CredentialsResponder;
import cws.impl.hist.TransHistoryResponder;
import cws.impl.hist.TransHistoryStmtResponder;
import cws.impl.trans.CurrentTransResponder;
import cws.log.Logger;

public class MessageReceiver extends HttpServlet implements Servlet {
	protected static HashMap responders = new HashMap();	
	static {
		MessageResponder responder = new ServiceResponder();
		responders.put("ping", responder);
		responders.put("echo", responder);
		responders.put("getStatus", responder);
		
		responders.put("getBalances", new BalancesResponder());//
		responders.put("verifyCredentials", new CredentialsResponder());//				
		responders.put("getAuthorizations", new AuthorizationResponder());//
		responders.put("getCurrentTransactions", new CurrentTransResponder()); //
		responders.put("getBillings", new BillingResponder());//
		responders.put("getTransHistoryStmt", new TransHistoryStmtResponder());// 
		responders.put("getTransHistory", new TransHistoryResponder());// 
		responders.put("getAllTransactions", new AllTransResponder());		
	}

	public void service(ServletRequest req, ServletResponse resp)
	throws ServletException, IOException {
		

					
//		Date date1read = new Date();
				
		// read the request
		BufferedReader reader = new BufferedReader(new InputStreamReader(req.getInputStream(), "UTF8"));		
		StringBuffer requestXML = new StringBuffer();
		String line;
		while ((line = reader.readLine()) != null) {
			requestXML.append(line);
		}		
		
//		Date date2read = new Date();
		
//		Logger.logTimeAndRequestMessage("READ REQUEST","", date1read, date2read);
				
				

				
		// process the request
		
		//prin kai meta ti grammh exw tous xronous
		String responseXML = processRequest(requestXML.toString());
		

		
		
		// trace
		StringBuffer trace = new StringBuffer();
		String newLine = System.getProperty("line.separator");
		trace.append("== MessageReceiver ====================================").append(newLine);
		trace.append("Request:  ").append(requestXML.toString()).append(newLine);
		trace.append("Response: ").append(responseXML).append(newLine);
		Logger.logInfo("CWS", trace.toString());



//		Date date1write = new Date();
		
		// return the response
		resp.setContentType("text/xml; charset=utf-8");
		int length = responseXML.length();
		resp.setContentLength(length);
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(resp.getOutputStream(), "UTF8"), 2*length);
		writer.write(responseXML);
		writer.flush();
		writer.close();
		
//		Date date2write = new Date();
//		
//		Logger.logTimeAndRequestMessage("WRITE RESPONSE","", date1write, date2write);
		
		int gg=99;

	}	
	
	protected String processRequest(String requestXML) {
		int[] positions = new int[2];
		String args;
		
		args = XmlTools.parseTag(requestXML, "Body", positions);
		
		String message = XmlTools.extractMessageName(args);
		 			
		String dataXML = XmlTools.parseTag(args, message, positions);
		args = args.substring(positions[1]);	
		String extraData = null;
		if (args.indexOf("Array") >= 0)
			extraData = XmlTools.parseTag(args, "Array", positions);		
		
		MessageResponder responder = (MessageResponder) responders.get(message);
		String responseXML = "";
		try { 		
			responseXML = responder.processRequest(message, dataXML, extraData);
		}
		catch (Exception ex) {
			Logger.logException(ex);
		}
				
		return responseXML;
	}		
			
						
}
