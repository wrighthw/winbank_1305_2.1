package cws;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Vector;
import java.util.Date;

import cws.log.Logger;

public abstract class MessageResponder {

	public abstract String processRequest(String message, String dataXML, String extraDataXML) throws Exception;
	
	protected String[] parseStringArray(String itemsXML) {
		Vector items = new Vector();
		int[] positions = new int[2];
		String item;
	
		while (itemsXML.length() > 0) { 		
			item = XmlTools.parseTag(itemsXML, "item", positions);
			if (item == null)
				item = XmlTools.parseTag(itemsXML, "Item", positions);
			if (item == null)
				break;
									
			items.add(item);
			
			itemsXML = itemsXML.substring(positions[1]);
		}		
	
		int count = items.size();
		String[] array = new String[count];
		for (int i = 0; i < count; i++)
			array[i] = (String) items.elementAt(i);
		return array;
	}
		
	protected void appendInteger(StringBuffer dataXML, String name, int value) {		
		dataXML.append("<").append(name).append(" xsi:type=\"xsd:int\">");
		
		String strValue = String.valueOf(value);
		dataXML.append(strValue);
		
		dataXML.append("</").append(name).append(">");
	}

	protected void appendDecimal(StringBuffer dataXML, String name, BigDecimal value) {
		if (value == null)
			dataXML.append("<").append(name).append(" xsi:type=\"soapenc:decimal\"/>");
		else {		
			dataXML.append("<").append(name).append(" xsi:type=\"soapenc:decimal\">");
			
			String strValue = amountToString(value);
			dataXML.append(strValue);
			
			dataXML.append("</").append(name).append(">");
		}
	}

	protected void appendDate(StringBuffer dataXML, String name, Date value) {				
				
		dataXML.append("<").append(name).append(" xsi:type=\"xsd:dateTime\">");
		
		String strValue = dateToString(value);
		dataXML.append(strValue);
		
		dataXML.append("</").append(name).append(">");		
	}

	protected void appendString(StringBuffer dataXML, String name, String value) {
		if (value == null || (value != null && value.equals("")))
			dataXML.append("<").append(name).append(" xsi:type=\"xsd:string\"/>");
		else {
			dataXML.append("<").append(name).append(" xsi:type=\"xsd:string\">");
			dataXML.append(value);		
			dataXML.append("</").append(name).append(">");
		}
	}

	protected void appendBoolean(StringBuffer dataXML, String name, boolean value) {
		dataXML.append("<").append(name).append(" xsi:type=\"xsd:boolean\">");
		
		String strValue = String.valueOf(value);
		dataXML.append(strValue);
		
		dataXML.append("</").append(name).append(">");
	}
			
	protected String dateToString(Date date) {	
		if (date == null)
			date = new Date(0);
			
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = null;  
		try {					
			dateString = formatter.format(date);
		}
		catch (Exception ex) {	
			Logger.logException(ex);				
		}
		return dateString;
	}

	protected Date stringToDate(String dateString) {			

//mits		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS");
//mits		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;  
		try {					
			date = formatter.parse(dateString);
		}
		catch (Exception ex) {					
			Logger.logException(ex);
		}
		return date;
	}	

	protected final String AMOUNT_FORMAT = "########0.00";
	
	protected DecimalFormat getDecimalFormatter() {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator('.');
		symbols.setGroupingSeparator('\0');
		DecimalFormat formatter = new DecimalFormat(AMOUNT_FORMAT, symbols);
		formatter.setGroupingUsed(false);
		
		return formatter;
	}
	
	protected String amountToString(BigDecimal amount) {	
		DecimalFormat formatter = getDecimalFormatter();
		
		String amountString = null;  
		try {					
			amountString = formatter.format(amount);
		}
		catch (Exception ex) {					
			Logger.logException(ex);
		}
		return amountString;
	}

	protected BigDecimal stringToAmount(String amountString) {	
		DecimalFormat formatter = getDecimalFormatter();
				
		BigDecimal amount = null;  
		try {								
			amount = new BigDecimal(formatter.parse(amountString).doubleValue());
		}
		catch (Exception ex) {
			Logger.logException(ex);					
		}
		return amount;
	}
	
	protected String encodeString(String data) {
		data = data.trim();
		StringBuffer encoded = new StringBuffer();
				
		char[] chars = data.toCharArray();
		int count = chars.length;
		char chr;
		for (int c = 0; c < count; c++) {
			chr = chars[c];
			if ((chr >= 'a' && chr <= 'z') 
				|| (chr >= 'A' && chr <= 'Z')
				|| (chr >= '0' && chr <= '9')
				|| (chr == ' ')
				|| (chr == '.')) {
				
				encoded.append(chr);
			}
			else {							
				encoded.append("&#").append((int) chr).append(";");
			}								
		}
		
		return encoded.toString();	
	}
	
	protected String decodeString(String data) {
		StringBuffer decoded = new StringBuffer();
		
		char[] chars = data.toCharArray();
		int count = chars.length;
		char chr;
		String value;
		int c2;
		for (int c = 0; c < count; c++) {
			chr = chars[c];
			if (chr == '&' && c < count-1 && chars[c+1] == '#') {
				c2 = c+2;
				while (c2 < count && chars[c2] != ';')
					c2++;
				if (chars[c2] == ';') {
					value = String.valueOf(chars, c+2, c2-(c+2));
					decoded.append((char) Integer.parseInt(value));											
					c = c2;
				}
				else
					decoded.append(chr);
			}
			else							
				decoded.append(chr);					
		}		
		
		return decoded.toString();
	}		
	
	protected String generateResponseXML(String resultName, String valueName, String value) {	
		StringBuffer xml = new StringBuffer();

		xml.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
			xml.append("<soap:Envelope"); 
				xml.append(" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\""); 
				xml.append(" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\""); 
				xml.append(" xmlns:tns=\"http://cws\""); 				
				xml.append(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""); 
				xml.append(" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">");
										
				xml.append("<soap:Body soapenc:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">");
				if (value != null) {				
					xml.append("<tns:").append(resultName).append(">");
						xml.append("<tns:").append(valueName).append(">");
						xml.append(value);
						xml.append("</tns:").append(valueName).append(">");
					xml.append("</tns:").append(resultName).append(">");				
				}
				xml.append("</soap:Body>");
			
			xml.append("</soap:Envelope>");
				
		return xml.toString();
	}		

	protected String generateResponseXMLArray(String resultName, String valueName, String nameSpace,
												String elementType, int size, 
												String multiRefs) {	
		StringBuffer xml = new StringBuffer();

		xml.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
			xml.append("<soap:Envelope"); 				 
				xml.append(" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"");
				xml.append(" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\"");
				xml.append(" xmlns:tns=\"").append(nameSpace).append("\""); 				 				
				xml.append(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""); 
				xml.append(" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">");									

				xml.append("<soap:Body soapenc:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">");	

					xml.append("<tns:").append(resultName).append(" xmlns=\"http://cws\">");
						xml.append("<tns:").append(valueName).append(" ")
							.append("xsi:type=\"soapenc:Array\" ")							.append("soapenc:arrayType=\"tns:")
								.append(elementType).append("[").append(size).append("]\">");																				

						if (size > 0) {
							for (int i = 0; i < size; i++) {
								xml.append("<item href=\"#id").append(i).append("\"/>");
							}
						}
						xml.append("</tns:").append(valueName).append(">");
						
					xml.append("</tns:").append(resultName).append(">");
						
					xml.append(multiRefs);											
									
				xml.append("</soap:Body>");
				
			xml.append("</soap:Envelope>");
					
		return xml.toString();
	}		

	protected void appendMultiRefStart(StringBuffer dataXML, int id, String elementType) {
		dataXML.append("<tns:").append(elementType).append(" id=\"id").append(id).append("\" ")				 
				.append("xsi:type=\"tns:").append(elementType).append("\">");				
	}		

	protected void appendMultiRefEnd(StringBuffer dataXML, String elementType) {
		dataXML.append("</tns:").append(elementType).append(">");				 				
	}		
	
	protected String generateResponseXMLStructure(String resultName, String valueName, String nameSpace,												 
												String structType, String values) {	
		StringBuffer xml = new StringBuffer();

		xml.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
			xml.append("<soap:Envelope"); 				 
				xml.append(" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"");
				xml.append(" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\""); 				 				
				xml.append(" xmlns:tns=\"").append(nameSpace).append("\""); 							
				xml.append(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""); 
				xml.append(" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">");									

				xml.append("<soap:Body soapenc:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">");	

					xml.append("<tns:").append(resultName).append(" xmlns=\"http://cws\">");
						xml.append("<tns:").append(valueName).append(" href=\"#id0\"/>");						
					xml.append("</tns:").append(resultName).append(">");
							
					appendMultiRefStart(xml, 0, structType);
					xml.append(values);
					appendMultiRefEnd(xml, structType);											
									
				xml.append("</soap:Body>");
				
			xml.append("</soap:Envelope>");
					
		return xml.toString();
	}		
}
