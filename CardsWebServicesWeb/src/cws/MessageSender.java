package cws;

import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

public class MessageSender {
	
	public static String sendMessage(URL url, String requestXML) {
		String responseXML = null;
		try {
			
			// setup connection
			URLConnection con = url.openConnection();			
			con.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
			con.setRequestProperty("Expect", "100-continue");
			con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; MS Web Services Client Protocol 1.1.4322.573)");
			con.setRequestProperty("SOAPAction", "http://cws");
			
			// send message			
			con.setDoOutput(true);			
			OutputStreamWriter out = new OutputStreamWriter(new BufferedOutputStream(con.getOutputStream()), "UTF8");						 								
			out.write(requestXML);
			out.write("\r\n");
			out.flush();
			out.close();
			
			// retrieve response
			InputStream is = con.getInputStream();							
			LineNumberReader reader = new LineNumberReader(new InputStreamReader(is, "UTF8"));		
			StringBuffer responseBuffer = new StringBuffer();
			String line;			
			while ((line = reader.readLine()) != null)
				responseBuffer.append(line);
			is.close();			
			responseXML = responseBuffer.toString();	
			
			// trace
			StringBuffer trace = new StringBuffer();
			String newLine = System.getProperty("line.separator");
			trace.append("Request:  ").append(requestXML).append(newLine);
			trace.append("Response: ").append(responseXML).append(newLine);
			System.out.println(trace);
		}
		catch (Exception ex) {			
			responseXML = ex.toString();
		}

		return responseXML;		
	}
	
	public static String sendGetBalances() {
		String xml = 
			"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<soapenv:Envelope " +
					"xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" " +		
					"xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" " +		
					"xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" " +		
					"xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
			
					"<soapenv:Body soapenc:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">" +
						"<getBalances xmlns=\"http://cws\">" +
							"<participantID xsi:type=\"xsd:string\" xmlns=\"\">002</participantID>" +
							"<cardNos xsi:type=\"soapenc:Array\" soapenc:arrayType=\"xsd:string[1]\" xmlns:ns155240237=\"http://cws\" xmlns=\"\">" +
								"<item>4908460338614010</item>" +
							"</cardNos>" +
						"</getBalances>" +
					"</soapenv:Body>" +
				"</soapenv:Envelope>";

		String result = "";
		try {
			URL url = new URL("http://localhost:9080/cwsweb/services/CardsWebService");
			result = sendMessage(url, xml);
		}
		catch (Exception ex) {
			result = ex.toString();
		}
		return result;
		
	}
	
	public static String sendGetAllTrans() {
		String xml = 
			"<?xml version=\"1.0\" encoding=\"UTF-8\"?><SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"><SOAP-ENV:Body><ns0:getAllTransactions xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:ns=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:ns0=\"http://cws\" ns:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"><participantID xmlns:ns0=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns=\"\" xsi:type=\"xs:string\">004</participantID><accountNo xmlns:ns0=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns=\"\" xsi:type=\"xs:string\">4117111020261001</accountNo><cardNo xmlns:ns0=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns=\"\" xsi:type=\"xs:string\">4117111020261001</cardNo><billCurrencyCode xmlns:ns0=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns=\"\" xsi:type=\"xs:string\">975</billCurrencyCode><dateFrom xmlns:ns0=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns=\"\" xsi:type=\"xs:date\">2009-03-02</dateFrom><dateTo xmlns:ns0=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns=\"\" xsi:type=\"xs:date\">2009-06-02</dateTo><sortByCardNo xmlns:ns0=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns=\"\" xsi:type=\"xs:boolean\">false</sortByCardNo><start xmlns:ns0=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns=\"\" xsi:type=\"xs:int\">1</start><count xmlns:ns0=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns=\"\" xsi:type=\"xs:int\">10</count></ns0:getAllTransactions></SOAP-ENV:Body></SOAP-ENV:Envelope>";

		String result = "";
		try {
			URL url = new URL("http://localhost:9080/cwsweb/services/CardsWebService");
			result = sendMessage(url, xml);
		}
		catch (Exception ex) {
			result = ex.toString();
		}
		return result;
		
	}

}
