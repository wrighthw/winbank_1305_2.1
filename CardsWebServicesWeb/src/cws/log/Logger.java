package cws.log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {
	static String filenameLog = "cws.log";	
	static String filenameTimeLog = "cws_time_request.log";	
	static String defaultContext = "CWS";
			
	public static void logInfo(String context, String message) {		
		logMessage(context, "I", message);
	}

	public static void logWarning(String context, String message) {
		logMessage(context, "W", message);
	}

	public static void logError(String context, String message) {
		logMessage(context, "E", message);
	}
	
	protected static void logMessage(String context, String type, String message) {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		String timestamp = format.format(new Date());
			
		StringBuffer line = new StringBuffer();
		line.append("\n")
			.append(timestamp)
			.append(" [");
	
		context = context.toUpperCase();
		if (context.length() > 10)
			line.append(context.substring(0, 9));
		else if (context.length() < 10) {
			line.append(context);
			int count = 10 - context.length();
			for (int i = 0; i < count; i++)
				line.append(" ");
		}				
		
		line.append("] ")
			.append(type.substring(0, 1))
			.append(" - ").append(message);
		
		try {			
			File fileLog = new File(filenameLog);
			if (fileLog.length() > 262144) {
				File fileLogOld = new File(filenameLog + ".old");
				if (fileLogOld.exists())
					fileLogOld.delete();
				fileLog.renameTo(fileLogOld);
			}				
		
			FileOutputStream fosLog = new FileOutputStream(filenameLog, true);
			OutputStreamWriter oswLog = new OutputStreamWriter(fosLog, "iso8859-7");
			oswLog.write(line.toString());
			oswLog.close();
			fosLog.close(); 
		}
		catch (Exception ex) {
			System.err.println("FLEX: Cannot write to log file");
			ex.printStackTrace();
		}
	}
	
//	public synchronized static void  logTimeAndRequestMessage(String requestName, String otherInfo, Date timeIn, Date timeOut) {
//	
//		String type = "I";
//		String context = "CWS";
//		
//		StringBuffer line = new StringBuffer();
//		line.append("\n");
//		
//		SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");// hh:mm:ss
//		SimpleDateFormat formatTime = new SimpleDateFormat("hh:mm:ss.S");// 
//		
//		SimpleDateFormat formatDateInt = new SimpleDateFormat("yyyyMMdd");// hh:mm:ss
//		SimpleDateFormat formatTimeInt = new SimpleDateFormat("hhmmssS");//
//				
//				
//		String strDate = formatDate.format(new Date());
//		
//		String strDateInt = formatDateInt.format(new Date());		
//		
//		String strTimeIn = formatTime.format(timeIn);
//		String strTimeOut = formatTime.format(timeOut);
//		
//		String strTimeInInt = formatTimeInt.format(timeIn);
//		String strTimeOutInt = formatTimeInt.format(timeOut);
//
//		
//		
//		line.append(strDate+"\t");
//		line.append(strDateInt+"\t");
//		
//		line.append(strTimeIn+"\t");
//		line.append(strTimeInInt+"\t");
//		
//		line.append(strTimeOut+"\t");
//		line.append(strTimeOutInt+"\t");
//		
//		line.append(requestName+"\t");
//		
//		line.append(otherInfo);
//		
//		
//		
//		
//		try {			
//			File fileLog = new File(filenameTimeLog);
//			if (fileLog.length() > 262144) {
//				File fileLogOld = new File(filenameTimeLog + ".old");
//				if (fileLogOld.exists())
//					fileLogOld.delete();
//				fileLog.renameTo(fileLogOld);
//			}				
//	
//			FileOutputStream fosLog = new FileOutputStream(filenameTimeLog, true);
//			OutputStreamWriter oswLog = new OutputStreamWriter(fosLog, "iso8859-7");
//			oswLog.write(line.toString());
//			oswLog.close();
//			fosLog.close(); 
//		}
//		catch (Exception ex) {
//			System.err.println("FLEX: Cannot write to log file");
//			ex.printStackTrace();
//		}
//		
//		
//		
//	}
	
//	protected synchronized static String buildMessage(Date time1, Date time2, Date time3, Date time4)
//	{
//		String message ="";
//	
//		SimpleDateFormat format_date = new SimpleDateFormat("dd/MM/yyyy");
//		SimpleDateFormat format_time = new SimpleDateFormat("hh:mm:ss");
//	
//		String yymmddDate = format_date.format(new Date());
//	
//		String wra1 = format_time.format(time1);
//		String wra2 = format_time.format(time2);
//		String wra3 = format_time.format(time3);
//		String wra4 = format_time.format(time4);
//			
//		return message;
//	}

	public static void logException(Exception ex) {
		SystemMessage msg = new SystemMessage(ex);
		logException(msg);
	}

	public static void logException(SystemMessage msg) {
		StringBuffer msgLog = new StringBuffer();
		
		msgLog.append("\n");
		
		msgLog.append("[").append(msg.getMessage()).append("]\n");
	
		msgLog.append(msg.getCallStack().toString());		
	
		logError(defaultContext, msgLog.toString());		
	}

	protected static String throwableToString(Throwable throwable)
	{
		StringWriter writer = new StringWriter();
		PrintWriter printer = new PrintWriter(writer, true);
		throwable.printStackTrace(printer);
		printer.flush();

		return writer.toString();
	}
}
