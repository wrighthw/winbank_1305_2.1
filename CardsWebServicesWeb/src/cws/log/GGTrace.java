/*
 * Created on 11 ��� 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package cws.log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author abc044
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class GGTrace {
	
	static String filenameLog = "cws_time_request.log";
	
	public static String extraInfo = "";
//	static String defaultContext = "CWS";
	
	
	public static void logInfo(String context, String message) {		
		logMessage(context, "I", message);
	}
	
	protected static void logMessage(String context, String type, String message) {
//		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		String timestamp = format.format(new Date());
			
		StringBuffer line = new StringBuffer();
		line.append("\n")
//			.append(timestamp)
			.append(" [");
	
		context = context.toUpperCase();
		if (context.length() > 10)
			line.append(context.substring(0, 9));
		else if (context.length() < 10) {
			line.append(context);
			int count = 10 - context.length();
			for (int i = 0; i < count; i++)
				line.append(" ");
		}				
		
		line.append("] ")
			.append(type.substring(0, 1))
			.append(" - ").append(message);
		
		try {			
			File fileLog = new File(filenameLog);
			if (fileLog.length() > 262144) {
				File fileLogOld = new File(filenameLog + ".old");
				if (fileLogOld.exists())
					fileLogOld.delete();
				fileLog.renameTo(fileLogOld);
			}				
		
			FileOutputStream fosLog = new FileOutputStream(filenameLog, true);
			OutputStreamWriter oswLog = new OutputStreamWriter(fosLog, "iso8859-7");
			oswLog.write(line.toString());
			oswLog.close();
			fosLog.close(); 
		}
		catch (Exception ex) {
			System.err.println("FLEX: Cannot write to log file");
			ex.printStackTrace();
		}
	}
	
	
	public static void writeLine(Date time1, Date time2, Date time3, Date time4)
	{
		
	}
	
	protected static String buildMessage(Date time1, Date time2, Date time3, Date time4)
	{
		String message ="";
		
		SimpleDateFormat format_date = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat format_time = new SimpleDateFormat("hh:mm:ss");
		
		String yymmddDate = format_date.format(new Date());
		
		String wra1 = format_time.format(time1);
		String wra2 = format_time.format(time2);
		String wra3 = format_time.format(time3);
		String wra4 = format_time.format(time4);
				
		return message;
	}
	
	
	


}
