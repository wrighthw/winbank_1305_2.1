package cws.log;

import java.io.Serializable;
import java.rmi.RemoteException;

import javax.ejb.EJBException;

public class SystemMessage implements Serializable {
	String code;	
	String message;	
	CallStack stackCall;

	public SystemMessage(String code) {
		this.code = code;
		this.message = code;
			
		this.stackCall = new CallStack();
	}

	public SystemMessage(String code, String message) {
		this.code = code;		
		this.message = removeNewLines(message);		

		this.stackCall = new CallStack();
	}

	public SystemMessage(Exception ex) {		
		this.code = ex.getClass().toString();		
		this.message = removeNewLines(getExceptionMessage(ex));
	
		this.stackCall = new CallStack(ex);
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public CallStack getCallStack() {
		return stackCall;
	}

	public final static int LITERAL = 0;
	public final static int LOOK_UP = 1;
	public final static int LOOK_UP_LIST = 2;

	public String toString() {
		return message;
	}
	
	protected String removeNewLines(String message) {
		message = message.replace('\n', '|');
		message = message.replace('\r', ' ');
		return message;
	}		

	protected String getStackTrace(Throwable ex)
	{
		String trace = Logger.throwableToString(ex);

		int pos = trace.indexOf('\n');
		if (pos < 0)
			pos = 0;

		return trace.substring(pos+1);
	}

	public static String getExceptionMessage(Exception ex) {
		Class classEx = ex.getClass();
	
		if (RemoteException.class.isAssignableFrom(classEx)) {
			ex = (Exception) ((RemoteException) ex).detail;
			classEx = ex.getClass();
		}

		// Yes! we need this code twice... IBM magic! :-)
		if (RemoteException.class.isAssignableFrom(classEx)) {
			ex = (Exception) ((RemoteException) ex).detail;
			classEx = ex.getClass();
		}

		if (EJBException.class.isAssignableFrom(classEx))
			ex = ((EJBException) ex).getCausedByException();

		String message = ex.getMessage();
		if (message == null) {
			message = ex.getClass().getName();
			if (message.indexOf("NullPointerException") >= 0)
				message = "NPE";
		}

		message = message.replace('\n', '|');

		return message;
	}
}
