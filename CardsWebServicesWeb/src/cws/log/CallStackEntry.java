package cws.log;

import java.io.Serializable;

public class CallStackEntry implements Serializable {
	protected String packageClass;
	protected String nameClass;
	protected String nameMethod;
	protected String nameFile;
	protected int noLine;

	public CallStackEntry(String lineTrace) {
		parseTraceLine(lineTrace);
	}

	public String getClassPackage() {
		return packageClass;
	}

	public String getClassName() {
		return nameClass;
	}

	public String getMethodName() {
		return nameMethod;
	}

	public String getFileName() {
		return nameFile;
	}

	public int getLineNo() {
		return noLine;
	}

	protected void parseTraceLine(String lineTrace) {
		//format: at flex.cop.ExternalTable.doLoad(ExternalTable.java:1356)

		int index;

		index = lineTrace.indexOf("at ");
		lineTrace = lineTrace.substring(index + 3);

		int indexParen = lineTrace.indexOf("(");
		String nameMethodQualified = (indexParen > 0) ? lineTrace.substring(0, indexParen) : lineTrace;
	
		index = nameMethodQualified.lastIndexOf(".");
		nameMethod = nameMethodQualified.substring(index + 1);
		nameMethodQualified = nameMethodQualified.substring(0, index);
	
		index = nameMethodQualified.lastIndexOf(".");
		nameClass = nameMethodQualified.substring(index + 1);
		packageClass = nameMethodQualified.substring(0, index);

		if (indexParen > 0) {	
			String nameFileFull = lineTrace.substring(indexParen + 1, lineTrace.length() - 1);
			index = nameFileFull.indexOf(":");
			if (index > 0) {
				nameFile = nameFileFull.substring(0, index);
				noLine = Integer.parseInt(nameFileFull.substring(index + 1));
			}
			else {
				nameFile = nameFileFull;
				noLine = 0;
			}
		}
		else {
			nameFile = "";
			noLine = 0;
		}
	}
	
	public String toString() {
		StringBuffer str = new StringBuffer();
		
		str.append(packageClass);
		str.append(".").append(nameClass);
		str.append(".").append(nameMethod);
		str.append("(").append(nameFile);
		if (noLine > 0) {
			str.append(":").append(noLine);
		}
		str.append(")");
	
		return str.toString();
	}
}