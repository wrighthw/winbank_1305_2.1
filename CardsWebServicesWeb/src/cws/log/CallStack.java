package cws.log;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.StringTokenizer;
import java.util.Vector;

public class CallStack implements Serializable {

	protected Vector entries;

	public CallStack() {
		String trace = getStackTrace();
		int index = trace.indexOf("at ");
		entries = new Vector();
		if (index >= 0)
			parseTrace(trace.substring(index));
	}

	public CallStack(Throwable t) {
		String trace = throwableToString(t);
		int index = trace.indexOf("at ");
		entries = new Vector();
		if (index >= 0)
			parseTrace(trace.substring(index));
	}

	public Vector getCallStackEntries() {
		return entries;
	}

	public String toString() {				
		StringBuffer str = new StringBuffer();
		
		CallStackEntry entry;
		int count = entries.size();
		for (int e = 0; e < count; e++) {
			entry = (CallStackEntry) entries.elementAt(e);
		
			str.append("        ");
			str.append(entry.getClassPackage());
			str.append(".").append(entry.getClassName());
			str.append(".").append(entry.getMethodName());
			str.append("(").append(entry.getFileName());
			if (entry.getLineNo() > 0) {
				str.append(":").append(entry.getLineNo());
			}
			str.append(")");
			str.append("\n");
		}
	
		return str.toString();
	}
	
	protected void parseTrace(String trace) {		
		StringTokenizer st = new StringTokenizer(trace, "\n\r\t");
		String line, packageLine;
		boolean isSystemPackage, isApplicationCode, isFlexCode;
		CallStackEntry entry;
		while (st.hasMoreTokens()) {
			line = st.nextToken();
			entry = new CallStackEntry(line);			
			if (line.indexOf("CallStack") < 0)
				entries.add(entry);
		}
	}

	protected String getStackTrace() {
		String trace = null;
		try {
			throw new Exception("trace");
		}
		catch (Exception ex) {
			trace = throwableToString(ex);
		}

		int pos = trace.indexOf('\n');
		if (pos > 0)
			pos = trace.indexOf('\n', pos + 1);
		if (pos < 0)
			pos = 0;

		return trace.substring(pos + 1);
	}

	protected String throwableToString(Throwable throwable) {
		StringWriter writer = new StringWriter();
		PrintWriter printer = new PrintWriter(writer, true);
		throwable.printStackTrace(printer);
		printer.flush();

		return writer.toString();
	}
}